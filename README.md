# Dev Storage Client

## Description
Dev Storage Client is a command-line interface (CLI) application for developers to store and manage development content files outside of version control systems. This tool integrates with Git, allowing users to link files to specific commit hashes. It leverages a decentralized peer-to-peer (P2P) graph for storing these files, ensuring a secure and distributed storage mechanism.

## Features
- **Snapshot Creation:** Generate snapshots of your development files, linked to Git commit hashes.
- **Decentralized Storage:** Store snapshots on a decentralized P2P graph for enhanced security and redundancy.
- **File Compression:** Uses Gzip compression to reduce storage space and improve transfer speeds.
- **Database Dumping:** Support for MySQL database dumping, allowing for comprehensive snapshotting of project states.
- **Configurable File Paths:** Customize the paths for content root and temporary storage.
- **Verbose Logging:** Integrated logging system with adjustable verbosity levels.


## Disclaimer
- **Project Status:** This project will not be developed any further.
- **Tests:** Some tests fail due to an unimplemented use case for pulling snapshots.
- **Purpose:** This project was developed as a proof-of-concept for a decentralized storage system built with [Holochain](https://www.holochain.org/) while learning Clean Architecture in Rust. It is not intended for production use.


## Use Cases
- **Create a packed snapshot of your project files**
    ```bash
    cargo run -- pack
    ```
- **Push the current snapshot to the decentralized storage**
    ```bash
    cargo run -- push
    ```

## Getting Started

### Prerequisites
- **Rust** - Install Rust using [these instructions](https://www.rust-lang.org/tools/install).

### Configuration
```javascript
// ./.dev-storage-client.json
{
    "relative_path_to_content_root": "../path/to/your/content/folder",
    "database": {
        "host": "<db-host>",
        "user": "<db-user>",
        "password": "<db-password>",
        "dbname": "<db-name>"
    },
    "holochain": {
        "keystore_url": "<keystore-url>",
        "keystore_passphrase": "<keystore-passphrase>",
        "happ_websocket_url": "<happ-websocket-url>"
    }
}
```

### Show help
```bash
cargo run -- help
```

### Run tests
```bash
cargo test
```

