use crate::domain::gateways::git_repository::GitRepository;

#[derive(Debug, Clone)]
pub struct GitRepositoryGatewayImpl;

impl GitRepositoryGatewayImpl {
    pub fn new() -> Self {
        GitRepositoryGatewayImpl
    }
}

impl GitRepository for GitRepositoryGatewayImpl {
    type Error = GitRepositoryGatewayImplError;

    fn get_current_commit_hash(&self) -> Result<String, Self::Error> {
        let output = match std::process::Command::new("git")
            .args(&["rev-parse", "HEAD"])
            .output()
        {
            Ok(output) => output,
            Err(e) => {
                return Err(GitRepositoryGatewayImplError::CommandExecution(format!(
                    "Failed to get current commit: {}",
                    e
                )))
            }
        };

        let hash = match String::from_utf8(output.stdout) {
            Ok(hash) => hash.trim().to_string(),
            Err(e) => {
                return Err(GitRepositoryGatewayImplError::OutputParsing(format!(
                    "Failed to parse commit hash: {}",
                    e
                )))
            }
        };

        Ok(hash)
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum GitRepositoryGatewayImplError {
    CommandExecution(String),
    OutputParsing(String),
}

impl std::fmt::Display for GitRepositoryGatewayImplError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            GitRepositoryGatewayImplError::CommandExecution(msg) => {
                write!(f, "Command execution error: {}", msg)
            }
            GitRepositoryGatewayImplError::OutputParsing(msg) => {
                write!(f, "Output parsing error: {}", msg)
            }
        }
    }
}

impl std::error::Error for GitRepositoryGatewayImplError {}
