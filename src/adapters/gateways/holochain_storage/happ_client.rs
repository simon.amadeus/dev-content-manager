use holochain_client::{AgentPubKey, AppWebsocket, ZomeCall};
use holochain_conductor_api::{AppInfo, CellInfo};
use holochain_types::prelude::{
    CapSecret, CellId, ExternIO, FunctionName, Nonce256Bits, Signature, Timestamp,
    ZomeCallUnsigned, ZomeName,
};
use lair_keystore_api::{dependencies::sodoken::BufRead, ipc_keystore_connect, LairClient};
use std::{
    fmt::{Display, Formatter},
    time::Duration,
};

#[derive(Clone)]
pub struct HappClient {
    keystore: LairClient,
    app_ws: AppWebsocket,
    app_info: AppInfo,
    cell_id: CellId,
    settings: HappClientSettings,
}

impl HappClient {
    pub async fn new(
        keystore_url: &str,
        keystore_passphrase: &str,
        app_url: String,
        app_id: String,
        dna_name: &str,
        settings: HappClientSettings,
    ) -> Result<HappClient, HappClientError> {
        let url = match url::Url::parse(keystore_url) {
            Ok(url) => url,
            Err(_) => {
                return Err(HappClientError::KeystoreError(
                    "Could not parse keystore url".to_string(),
                ))
            }
        };
        let keystore =
            match ipc_keystore_connect(url, BufRead::new_no_lock(keystore_passphrase.as_bytes()))
                .await
            {
                Ok(keystore) => keystore,
                Err(_) => {
                    return Err(HappClientError::KeystoreError(
                        "Could not connect to keystore".to_string(),
                    ))
                }
            };
        let mut app_ws = match AppWebsocket::connect(app_url).await {
            Ok(app_ws) => app_ws,
            Err(_) => {
                return Err(HappClientError::AppWebsocketError(
                    "Could not connect to app websocket".to_string(),
                ))
            }
        };
        let app_info: AppInfo =
            match app_ws.app_info(app_id).await.map_err(|_| {
                HappClientError::AppWebsocketError("Failed to get app info".to_string())
            })? {
                Some(app_info) => app_info,
                None => {
                    return Err(HappClientError::AppWebsocketError(
                        "App info is None".to_string(),
                    ))
                }
            };

        let cell_id: CellId = match app_info
            .cell_info
            .get(dna_name)
            .ok_or("Could not get cell info")
            .map_err(|err| HappClientError::CellInfoError(err.to_string()))?
            .first()
            .ok_or("Failed to get provisioned cell info")
            .map_err(|err| HappClientError::CellInfoError(err.to_string()))?
        {
            CellInfo::Provisioned(cell) => cell.to_owned().cell_id,
            _ => {
                return Err(HappClientError::CellInfoError(
                    "No provisioned cell found".to_string(),
                ))
            }
        };

        Ok(HappClient {
            keystore,
            app_ws,
            app_info,
            cell_id,
            settings,
        })
    }

    pub fn app_info(&self) -> &AppInfo {
        &self.app_info
    }

    pub fn cell_id(&self) -> &CellId {
        &self.cell_id
    }

    pub fn agent_pub_key(&self) -> &AgentPubKey {
        &self.cell_id.agent_pubkey()
    }

    fn create_unsigned_zome_call(
        &self,
        zome_name: ZomeName,
        zome_fn_name: FunctionName,
        payload: ExternIO,
    ) -> Result<ZomeCallUnsigned, HappClientError> {
        let mut bytes = [0; 32];
        if let Err(error) = getrandom::getrandom(&mut bytes) {
            return Err(HappClientError::NonceError(format!(
                "Failed to generate random nonce: {}",
                error
            )));
        }
        let nonce = Nonce256Bits::from(bytes);
        let now = Timestamp::now();
        let expires_at: Timestamp = match now + self.settings.refresh_nonce_after {
            Ok(expires_at) => expires_at,
            Err(err) => {
                return Err(HappClientError::TimestampError(format!(
                    "Failed to add duration to timestamp: {}",
                    err
                )))
            }
        };

        Ok(ZomeCallUnsigned {
            cell_id: self.cell_id.clone(),
            zome_name: zome_name,
            fn_name: zome_fn_name,
            payload,
            cap_secret: self.settings.cap_secret.clone(),
            provenance: self.agent_pub_key().clone(),
            nonce,
            expires_at,
        })
    }

    async fn sign_zome_call(
        &self,
        unsigned_zome_call: ZomeCallUnsigned,
    ) -> Result<ZomeCall, HappClientError> {
        let data_to_sign: std::sync::Arc<[u8]> = match unsigned_zome_call.data_to_sign() {
            Ok(data_to_sign) => data_to_sign,
            Err(err) => {
                return Err(HappClientError::SignatureError(format!(
                    "Failed to get data to sign: {}",
                    err
                )))
            }
        };

        let mut signing_pub_key = [0; 32];
        signing_pub_key.copy_from_slice(self.agent_pub_key().get_raw_32());

        let sig = match self
            .keystore
            .sign_by_pub_key(signing_pub_key.into(), None, data_to_sign)
            .await
        {
            Ok(sig) => sig,
            Err(err) => {
                return Err(HappClientError::SignatureError(format!(
                    "Failed to sign data: {}",
                    err
                )))
            }
        };

        let signature = Signature(*sig.0);

        Ok(ZomeCall {
            cell_id: unsigned_zome_call.cell_id,
            zome_name: unsigned_zome_call.zome_name,
            fn_name: unsigned_zome_call.fn_name,
            payload: unsigned_zome_call.payload,
            cap_secret: unsigned_zome_call.cap_secret,
            provenance: unsigned_zome_call.provenance,
            signature,
            nonce: unsigned_zome_call.nonce,
            expires_at: unsigned_zome_call.expires_at,
        })
    }

    pub async fn make_zome_call(
        &mut self,
        zome_name: ZomeName,
        zome_fn_name: FunctionName,
        payload: ExternIO,
    ) -> Result<ExternIO, HappClientError> {
        let unsigned_zome_call =
            self.create_unsigned_zome_call(zome_name, zome_fn_name, payload)?;

        let signed_zome_call = self.sign_zome_call(unsigned_zome_call).await?;

        let extern_io = match self.app_ws.call_zome(signed_zome_call).await {
            Ok(extern_io) => extern_io,
            Err(err) => {
                return Err(HappClientError::ZomeCallError(format!(
                    "Failed to call zome: {:#?}",
                    err
                )))
            }
        };

        Ok(extern_io)
    }
}

#[derive(Clone)]
pub struct HappClientSettings {
    refresh_nonce_after: Duration,
    cap_secret: Option<CapSecret>,
}

impl Default for HappClientSettings {
    fn default() -> Self {
        Self {
            refresh_nonce_after: Duration::from_secs(60),
            cap_secret: None,
        }
    }
}

#[derive(Debug)]
pub enum HappClientError {
    KeystoreError(String),
    AppWebsocketError(String),
    AppInfoError(String),
    CellInfoError(String),
    TimestampError(String),
    NonceError(String),
    SignatureError(String),
    ZomeCallError(String),
}

impl Display for HappClientError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            HappClientError::KeystoreError(msg) => write!(f, "Keystore error: {}", msg),
            HappClientError::AppWebsocketError(msg) => write!(f, "App websocket error: {}", msg),
            HappClientError::AppInfoError(msg) => write!(f, "App info error: {}", msg),
            HappClientError::CellInfoError(msg) => write!(f, "Cell info error: {}", msg),
            HappClientError::TimestampError(msg) => write!(f, "Timestamp error: {}", msg),
            HappClientError::NonceError(msg) => write!(f, "Nonce error: {}", msg),
            HappClientError::SignatureError(msg) => write!(f, "Signature error: {}", msg),
            HappClientError::ZomeCallError(msg) => write!(f, "Zome call error: {}", msg),
        }
    }
}
