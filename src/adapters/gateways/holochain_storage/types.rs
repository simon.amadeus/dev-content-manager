use crate::domain::{entities::file::FileKind, value_objects::Compression};
use holochain_types::prelude::{holochain_serial, EntryHash, SerializedBytes};
use serde::{Deserialize, Serialize};

// interface types
// all files
#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct GetFilesByCommitHashInput {
    pub commit_hash: String,
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct GetFilesByCommitHashOutput {
    pub files: Vec<FileDTO>,
}

// GetAllFileHashesInput is empty

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct GetAllFileHashesOutput {
    pub file_hashes: Vec<String>,
}

// file chunk
#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct StoreFileChunkInput {
    pub file_chunk: FileChunkDTO,
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct StoreFileChunkOutput {
    pub file_chunk_entry_hash: EntryHash,
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct GetFileChunkInput {
    pub file_chunk_entry_hash: EntryHash,
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct GetFileChunkOutput {
    pub file_chunk: FileChunkDTO,
}

// file
#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct StoreFileInput {
    pub commit_hash: String,
    pub file: FileDTO,
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct StoreFileOutput {
    pub file_entry_hash: EntryHash,
}

// integrity types
#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct FileDTO {
    pub kind: FileKindDTO,
    pub size: u64,
    pub file_hash: String,
    pub preserved_path: Vec<String>,
    pub compression: CompressionDTO,
    pub file_chunks: Vec<EntryHash>,
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub enum FileKindDTO {
    ContentFile,
    DatabaseStructureDump,
    DatabaseContentDump,
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub enum CompressionDTO {
    None,
    Gzip,
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone, PartialEq)]
pub struct FileChunkDTO {
    pub bytes: Vec<u8>,
}

// adapter implementations
impl From<&Compression> for CompressionDTO {
    fn from(compression: &Compression) -> Self {
        match compression {
            Compression::None => CompressionDTO::None,
            Compression::Gzip => CompressionDTO::Gzip,
        }
    }
}

impl From<&FileKind> for FileKindDTO {
    fn from(file_kind: &FileKind) -> Self {
        match file_kind {
            FileKind::ContentFile => FileKindDTO::ContentFile,
            FileKind::DatabaseStructureDump => FileKindDTO::DatabaseStructureDump,
            FileKind::DatabaseContentDump => FileKindDTO::DatabaseContentDump,
        }
    }
}
