pub mod happ_client;
pub mod types;
use self::happ_client::{HappClient, HappClientError};
use crate::adapters::gateways::holochain_storage::happ_client::HappClientSettings;
use crate::adapters::gateways::holochain_storage::types::{
    FileChunkDTO, FileDTO, GetAllFileHashesOutput, StoreFileChunkInput, StoreFileChunkOutput,
    StoreFileInput, StoreFileOutput,
};
use crate::domain::aggregates::snapshot::Snapshot;
use crate::domain::entities::file::File;
use crate::domain::gateways::remote_storage::RemoteStorage;
use crate::infrastructure::configuration::HolochainConfig;
use holochain_types::prelude::{EntryHash, ExternIO, FunctionName, ZomeName};
use indicatif::ProgressBar;
use std::collections::HashSet;
use std::fs::File as FsFile;
use std::io::Read;
use std::path::Path;

#[derive(Debug)]
pub struct HolochainGateway {
    config: HolochainGatewayConfig,
}

#[derive(Debug, Clone)]
pub struct HolochainGatewayConfig {
    pub keystore_url: String,
    pub keystore_passphrase: String,
    pub happ_websocket_url: String,
}

impl From<HolochainConfig> for HolochainGatewayConfig {
    fn from(config: HolochainConfig) -> Self {
        Self {
            keystore_url: config.keystore_url,
            keystore_passphrase: config.keystore_passphrase,
            happ_websocket_url: config.happ_websocket_url,
        }
    }
}

impl HolochainGateway {
    pub fn new(config: HolochainGatewayConfig) -> Self {
        Self { config }
    }
}

#[derive(Debug)]
pub enum HolochainGatewayError {
    StoreFileError(String),
    HappClientError(HappClientError),
}

impl std::error::Error for HolochainGatewayError {}

impl std::fmt::Display for HolochainGatewayError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            HolochainGatewayError::StoreFileError(message) => {
                write!(f, "failed to store file: {}", message)
            }
            HolochainGatewayError::HappClientError(error) => {
                write!(f, "happ client error: {}", error)
            }
        }
    }
}

impl From<HappClientError> for HolochainGatewayError {
    fn from(error: HappClientError) -> Self {
        Self::HappClientError(error)
    }
}

const CHUNK_SIZE: usize = 2_000_000;

fn read_into_chunks<P: AsRef<Path>>(path: P) -> std::io::Result<Vec<Vec<u8>>> {
    let mut file = FsFile::open(path)?;
    let mut buffer = vec![0; CHUNK_SIZE];
    let mut chunks = vec![];

    loop {
        let n = file.read(&mut buffer)?;
        if n == 0 {
            break;
        }
        chunks.push(buffer[..n].to_vec());
    }

    Ok(chunks)
}

impl RemoteStorage for HolochainGateway {
    type Error = HolochainGatewayError;

    fn push_snapshot(&self, backup: Snapshot) -> Result<(), Self::Error> {
        let runtime = match tokio::runtime::Runtime::new() {
            Ok(runtime) => runtime,
            Err(error) => {
                return Err(HolochainGatewayError::StoreFileError(format!(
                    "failed to create tokio runtime: {}",
                    error
                )))
            }
        };

        runtime.block_on(async {
            let mut happ_client = HappClient::new(
                &self.config.keystore_url,
                &self.config.keystore_passphrase,
                self.config.happ_websocket_url.clone(),
                "dev-storage".to_string(),
                "dev_storage",
                HappClientSettings::default(),
            )
            .await?;
            let zome_name = ZomeName::from("dev_storage");

            ////// get all existing file hashes from holochain
            let zome_fn_name = FunctionName::from("get_all_file_hashes");
            let input = ();
            let payload = match ExternIO::encode(input) {
                Ok(payload) => payload,
                Err(error) => {
                    return Err(HolochainGatewayError::StoreFileError(format!(
                        "failed to encode payload: {}",
                        error
                    )))
                }
            };
            let response: ExternIO = happ_client
                .make_zome_call(zome_name.clone(), zome_fn_name, payload)
                .await?;
            let output: GetAllFileHashesOutput = match response.decode() {
                Ok(output) => output,
                Err(error) => {
                    return Err(HolochainGatewayError::StoreFileError(format!(
                        "failed to decode response: {}",
                        error
                    )))
                }
            };
            //////

            let mut existing_file_hashes: HashSet<&str> =
                HashSet::with_capacity(output.file_hashes.len());
            for file_hash in output.file_hashes.iter() {
                existing_file_hashes.insert(file_hash);
            }

            let mut new_files: Vec<&File> = vec![];
            for file in backup.files() {
                if existing_file_hashes.contains(file.hash()) {
                    continue;
                } else {
                    new_files.push(file);
                }
            }
            if new_files.is_empty() {
                return Ok(());
            }

            let progress_bar = ProgressBar::new(new_files.len() as u64);
            for file in new_files.iter() {
                progress_bar.inc(1);

                let file_chunks = match read_into_chunks(file.path()) {
                    Ok(file_chunks) => file_chunks,
                    Err(error) => {
                        return Err(HolochainGatewayError::StoreFileError(format!(
                            "failed to read file into chunks: {}",
                            error
                        )))
                    }
                };

                ////// create file chunk entries
                let mut file_chunk_entry_hashes: Vec<EntryHash> = vec![];
                let zome_fn_name = FunctionName::from("store_file_chunk");
                let file_chunks_dto = file_chunks
                    .into_iter()
                    .map(|chunk| FileChunkDTO { bytes: chunk })
                    .collect::<Vec<FileChunkDTO>>();
                for file_chunk in file_chunks_dto {
                    let input = StoreFileChunkInput { file_chunk };
                    let payload = match ExternIO::encode(input) {
                        Ok(payload) => payload,
                        Err(error) => {
                            return Err(HolochainGatewayError::StoreFileError(format!(
                                "failed to encode store file chunk input: {}",
                                error
                            )))
                        }
                    };
                    let response: ExternIO = happ_client
                        .make_zome_call(zome_name.clone(), zome_fn_name.clone(), payload)
                        .await?;
                    let output: StoreFileChunkOutput = match response.decode() {
                        Ok(output) => output,
                        Err(error) => {
                            return Err(HolochainGatewayError::StoreFileError(format!(
                                "failed to decode store file chunk output: {}",
                                error
                            )))
                        }
                    };
                    file_chunk_entry_hashes.push(output.file_chunk_entry_hash);
                }
                //////
                ////// create file entry
                let zome_fn_name = FunctionName::from("store_file");
                let file_dto = FileDTO {
                    kind: file.kind().into(),
                    size: file.size(),
                    file_hash: file.hash().to_string(),
                    preserved_path: file.preserved_path().to_vec(),
                    compression: file.compression().into(),
                    file_chunks: file_chunk_entry_hashes,
                };
                let input = StoreFileInput {
                    commit_hash: backup.commit_hash_value().to_string(),
                    file: file_dto,
                };
                let payload = match ExternIO::encode(input) {
                    Ok(payload) => payload,
                    Err(error) => {
                        return Err(HolochainGatewayError::StoreFileError(format!(
                            "failed to encode store file input: {}",
                            error
                        )))
                    }
                };
                let response: ExternIO = happ_client
                    .make_zome_call(zome_name.clone(), zome_fn_name.clone(), payload)
                    .await?;
                let _output: StoreFileOutput = match response.decode() {
                    Ok(output) => output,
                    Err(error) => {
                        return Err(HolochainGatewayError::StoreFileError(format!(
                            "failed to decode store file output: {}",
                            error
                        )))
                    }
                };
                //////
            }

            progress_bar.finish_and_clear();

            Ok(())
        })?;

        Ok(())
    }

    fn pull_snapshot(
        &self,
        _commit_hash: &str,
        _tmp_dir: std::path::PathBuf,
    ) -> Result<Option<Snapshot>, Self::Error> {
        unimplemented!("pull_snapshot not implemented for HolochainGateway")
    }
}
