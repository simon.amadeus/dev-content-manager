use crate::application::use_cases::create_snapshot::CreateSnapshot;
use crate::application::use_cases::create_snapshot_package::CreateSnapshotPackage;
use crate::application::use_cases::create_snapshot_package::CreateSnapshotPackageError;
use crate::application::use_cases::push_snapshot_to_storage::PushSnapshotToStorage;
use crate::application::use_cases::push_snapshot_to_storage::PushSnapshotToStorageError;
use crate::domain::gateways::remote_storage::RemoteStorage;
use crate::domain::services::file_compressor::FileCompressor;
use clap::Parser;
use clap::Subcommand;
use std::error::Error;

pub struct CliController<P: CreateSnapshotPackage, S: PushSnapshotToStorage> {
    create_content_package_use_case: P,
    push_commit_related_files_to_storage_use_case: S,
    cli_args: CliArgs,
}

impl<P, S> CliController<P, S>
where
    P: CreateSnapshotPackage,
    S: PushSnapshotToStorage,
{
    pub fn new(
        create_content_package_use_case: P,
        push_commit_related_files_to_storage_use_case: S,
        cli_args: CliArgs,
    ) -> Self {
        Self {
            create_content_package_use_case,
            push_commit_related_files_to_storage_use_case,
            cli_args,
        }
    }

    pub fn execute(&mut self) -> Result<(), CliControllerError> {
        match &self.cli_args.subcommand {
            Command::CreateContentPackage => match self.create_content_package_use_case.execute() {
                Ok(_) => Ok(()),
                Err(err) => Err(CliControllerError::CreateContentPackageError(format!(
                    "{}",
                    err
                ))),
            },
            Command::PushFilesToStorage => {
                match self.push_commit_related_files_to_storage_use_case.execute() {
                    Ok(_) => Ok(()),
                    Err(err) => Err(CliControllerError::PushCommitRelatedFilesToStorageError(
                        format!("{}", err),
                    )),
                }
            }
        }
    }
}

#[derive(Debug)]
pub enum CliControllerError {
    CreateContentPackageError(String),
    PushCommitRelatedFilesToStorageError(String),
}

impl std::fmt::Display for CliControllerError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            CliControllerError::CreateContentPackageError(msg) => {
                write!(f, "Create content package error: {}", msg)
            }
            CliControllerError::PushCommitRelatedFilesToStorageError(msg) => {
                write!(f, "Push commit related files to storage error: {}", msg)
            }
        }
    }
}

impl Error for CliControllerError {}

impl<U, C> From<CreateSnapshotPackageError<U, C>> for CliControllerError
where
    U: CreateSnapshot,
    C: FileCompressor,
{
    fn from(error: CreateSnapshotPackageError<U, C>) -> Self {
        Self::CreateContentPackageError(error.to_string())
    }
}

impl<K, M> From<PushSnapshotToStorageError<K, M>> for CliControllerError
where
    K: RemoteStorage,
    M: CreateSnapshot,
{
    fn from(error: PushSnapshotToStorageError<K, M>) -> Self {
        Self::PushCommitRelatedFilesToStorageError(error.to_string())
    }
}

/// Utility to share project content with other developers
///
/// The main purpose of this tool is to share project content based on a git commit hash.
/// This is useful so that developers can share the same content for a given commit hash
/// without committing content into the git repository.
///
/// Required configuration parameters will be derived from files in the working directory.
/// If the extraction of parameters fails, manual provision is mandatory.
#[derive(Parser, Debug, Clone)]
#[command(author, version)]
#[command(propagate_version = true)]
pub struct CliArgs {
    #[command(subcommand)]
    pub subcommand: Command,
    /// Set the verbosity level (default: Info, -v: Debug, -vv: Trace)
    #[arg(short, action = clap::ArgAction::Count)]
    pub verbose: u8,
}

#[derive(Subcommand, Debug, Clone)]
pub enum Command {
    /// Create a content package only
    #[command(name = "pack")]
    CreateContentPackage,
    /// Push content files to remote storage
    #[command(name = "push")]
    PushFilesToStorage,
}
