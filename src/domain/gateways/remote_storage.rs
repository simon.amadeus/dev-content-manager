use std::path::PathBuf;

use crate::domain::aggregates::snapshot::Snapshot;

pub trait RemoteStorage {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn push_snapshot(&self, snapshot: Snapshot) -> Result<(), Self::Error>;
    fn pull_snapshot(
        &self,
        commit_hash: &str,
        tmp_dir: PathBuf,
    ) -> Result<Option<Snapshot>, Self::Error>;
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use std::cell::RefCell;

    #[derive(Debug, Clone)]
    pub struct MockStorageGateway {
        success: bool,
        snapshot: RefCell<Option<Snapshot>>,
        called_push_snapshot_count: RefCell<u8>,
        called_pull_snapshot_count: RefCell<u8>,
    }

    impl MockStorageGateway {
        pub fn new(success: bool) -> Self {
            Self {
                success,
                snapshot: RefCell::new(None),
                called_push_snapshot_count: RefCell::new(0),
                called_pull_snapshot_count: RefCell::new(0),
            }
        }

        pub fn called_push_snapshot_count(&self) -> u8 {
            *self.called_push_snapshot_count.borrow()
        }

        pub fn push_snapshot_was_called(&self) -> bool {
            self.called_push_snapshot_count() > 0
        }

        pub fn called_pull_snapshot_count(&self) -> u8 {
            *self.called_pull_snapshot_count.borrow()
        }

        pub fn pull_snapshot_was_called(&self) -> bool {
            self.called_pull_snapshot_count() > 0
        }
    }

    impl RemoteStorage for MockStorageGateway {
        type Error = MockStorageGatewayError;

        fn push_snapshot(&self, backup: Snapshot) -> Result<(), Self::Error> {
            if self.success {
                *self.called_push_snapshot_count.borrow_mut() += 1;

                *self.snapshot.borrow_mut() = Some(backup);
                Ok(())
            } else {
                Err(MockStorageGatewayError::StoreError)
            }
        }

        fn pull_snapshot(
            &self,
            _commit_hash: &str,
            _tmp_dir: PathBuf,
        ) -> Result<Option<Snapshot>, Self::Error> {
            if self.success {
                *self.called_pull_snapshot_count.borrow_mut() += 1;

                Ok(self.snapshot.borrow().clone())
            } else {
                Err(MockStorageGatewayError::StoreError)
            }
        }
    }

    #[derive(Debug)]
    pub enum MockStorageGatewayError {
        StoreError,
        PullError,
    }

    impl std::fmt::Display for MockStorageGatewayError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                MockStorageGatewayError::StoreError => write!(f, "StoreError"),
                MockStorageGatewayError::PullError => write!(f, "PullError"),
            }
        }
    }

    impl std::error::Error for MockStorageGatewayError {}

    pub struct MockStorageGatewayBuilder {
        success: bool,
        snapshot: Option<Snapshot>,
    }

    impl MockStorageGatewayBuilder {
        pub fn new() -> Self {
            Self {
                success: true,
                snapshot: None,
            }
        }

        pub fn with_error(mut self) -> Self {
            self.success = false;
            self
        }

        pub fn with_snapshot(mut self, snapshot: Snapshot) -> Self {
            self.snapshot = Some(snapshot);
            self
        }

        pub fn build(self) -> MockStorageGateway {
            MockStorageGateway::new(self.success)
        }
    }
}
