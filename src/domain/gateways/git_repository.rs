pub trait GitRepository {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn get_current_commit_hash(&self) -> Result<String, Self::Error>;
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use std::cell::RefCell;

    #[derive(Debug, Clone)]
    pub struct MockGitRepositoryGateway {
        success: bool,
        current_commit_hash: Option<String>,
        called_get_current_commit_hash_count: RefCell<u8>,
    }

    impl MockGitRepositoryGateway {
        pub fn new(success: bool, current_commit_hash: Option<String>) -> Self {
            Self {
                success,
                current_commit_hash,
                called_get_current_commit_hash_count: RefCell::new(0),
            }
        }

        pub fn current_commit_hash(&self) -> Option<String> {
            self.current_commit_hash.clone()
        }

        pub fn get_current_commit_hash_was_called(&self) -> bool {
            *self.called_get_current_commit_hash_count.borrow() > 0
        }

        pub fn called_get_current_commit_hash_count(&self) -> u8 {
            *self.called_get_current_commit_hash_count.borrow()
        }
    }

    impl GitRepository for MockGitRepositoryGateway {
        type Error = MockGitRepositoryGatewayError;

        fn get_current_commit_hash(&self) -> Result<String, Self::Error> {
            *self.called_get_current_commit_hash_count.borrow_mut() += 1;

            if self.success {
                Ok(self
                    .current_commit_hash
                    .clone()
                    .expect("no commit provided"))
            } else {
                Err(MockGitRepositoryGatewayError::GetCurrentCommitError)
            }
        }
    }

    #[derive(Debug)]
    pub enum MockGitRepositoryGatewayError {
        GetCurrentCommitError,
    }

    impl std::fmt::Display for MockGitRepositoryGatewayError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                MockGitRepositoryGatewayError::GetCurrentCommitError => {
                    write!(f, "GetCurrentCommitError")
                }
            }
        }
    }

    impl std::error::Error for MockGitRepositoryGatewayError {}

    pub struct MockGitRepositoryGatewayBuilder {
        success: bool,
        current_commit_hash: Option<String>,
    }

    impl MockGitRepositoryGatewayBuilder {
        pub fn new() -> Self {
            Self {
                success: true,
                current_commit_hash: None,
            }
        }

        pub fn with_error(mut self) -> Self {
            self.success = false;
            self
        }

        pub fn with_current_commit_hash(mut self, current_commit_hash: String) -> Self {
            self.current_commit_hash = Some(current_commit_hash);
            self
        }

        pub fn build(self) -> MockGitRepositoryGateway {
            MockGitRepositoryGateway::new(self.success, self.current_commit_hash)
        }
    }
}
