use crate::domain::value_objects::Compression;
use std::path::PathBuf;

#[derive(Debug, Clone, PartialEq)]
pub struct File {
    path: PathBuf,
    size: u64,
    hash: String,
    kind: FileKind,
    compression: Compression,
    preserved_path: Vec<String>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum FileKind {
    ContentFile,
    DatabaseContentDump,
    DatabaseStructureDump,
}

impl File {
    pub fn new(
        path: PathBuf,
        size: u64,
        hash: String,
        kind: FileKind,
        compression: Compression,
        preserved_path: Vec<String>,
    ) -> Self {
        File {
            path,
            size,
            hash,
            kind,
            compression,
            preserved_path,
        }
    }

    pub fn path(&self) -> &PathBuf {
        &self.path
    }

    pub fn size(&self) -> u64 {
        self.size
    }

    pub fn hash(&self) -> &str {
        &self.hash
    }

    pub fn kind(&self) -> &FileKind {
        &self.kind
    }

    pub fn compression(&self) -> &Compression {
        &self.compression
    }

    pub fn preserved_path(&self) -> &[String] {
        &self.preserved_path
    }

    pub fn set_path(&mut self, path: PathBuf) {
        self.path = path;
    }
}
