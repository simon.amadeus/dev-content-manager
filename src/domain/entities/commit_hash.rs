#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct CommitHash {
    hash: String,
}

impl CommitHash {
    pub fn new(hash: String) -> Self {
        CommitHash { hash }
    }

    pub fn hash(&self) -> &str {
        &self.hash
    }
}
