use crate::domain::entities::commit_hash::CommitHash;
use crate::domain::entities::file::{File, FileKind};
use std::path::{Path, PathBuf};

// Do not derive Clone!
// This is important due to the Drop implementation.
// If TempDirectory would be cloned, the Drop implementation would be called twice.
#[derive(Debug)]
struct TempDirectory {
    path: PathBuf,
}

impl TempDirectory {
    fn exists(&self) -> bool {
        self.path.exists()
    }

    fn join(&self, path: impl AsRef<Path>) -> PathBuf {
        self.path.join(path)
    }

    fn path(&self) -> &PathBuf {
        &self.path
    }
}

impl Drop for TempDirectory {
    fn drop(&mut self) {
        if self.exists() {
            if let Err(error) = std::fs::remove_dir_all(self.path()) {
                eprintln!("Failed to remove temporary directory: {}", error);
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct Flattened;
#[derive(Debug, Clone)]
pub struct Structured;

#[derive(Debug, Clone)]
pub struct Snapshot<State = Flattened> {
    state: std::marker::PhantomData<State>,
    pub commit_hash: CommitHash,
    pub files: Vec<File>,
    tmp_dir: std::sync::Arc<TempDirectory>,
}

impl Snapshot {
    pub fn new(commit_hash: CommitHash, tmp_dir: PathBuf) -> Snapshot<Flattened> {
        let tmp_dir = std::sync::Arc::new(TempDirectory { path: tmp_dir });
        Snapshot {
            state: std::marker::PhantomData,
            commit_hash,
            files: Vec::new(),
            tmp_dir,
        }
    }

    pub fn with_capacity(
        commit_hash: CommitHash,
        tmp_dir: PathBuf,
        capacity: usize,
    ) -> Snapshot<Flattened> {
        let tmp_dir = std::sync::Arc::new(TempDirectory { path: tmp_dir });
        Snapshot {
            state: std::marker::PhantomData,
            commit_hash,
            files: Vec::with_capacity(capacity),
            tmp_dir,
        }
    }
}

impl Snapshot<Flattened> {
    pub fn add_file(&mut self, file: File) -> Result<(), std::io::Error> {
        let tmp_file_name = self.files.len().to_string();
        let target_file = self.tmp_dir.join(tmp_file_name);

        match file.kind() {
            FileKind::ContentFile => {
                // copies files -> keeps original file
                std::fs::copy(file.path(), target_file.clone())?;
            }
            FileKind::DatabaseContentDump | FileKind::DatabaseStructureDump => {
                // moves file -> removes original file
                std::fs::rename(file.path(), target_file.clone())?;
            }
        }

        let file = File::new(
            target_file,
            file.size(),
            file.hash().to_string(),
            file.kind().clone(),
            file.compression().clone(),
            file.preserved_path().to_vec(),
        );
        self.files.push(file);

        Ok(())
    }

    pub fn restore_directory_structure(self) -> Result<Snapshot<Structured>, std::io::Error> {
        let mut new_files: Vec<File> = Vec::new();
        for file in self.files() {
            let mut file = file.clone();
            let mut path: PathBuf = file.preserved_path().iter().collect();
            path = self.tmp_dir.join(path);
            let parent = match path.parent() {
                Some(parent) => parent,
                None => {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        "Failed to restore directory structure",
                    ))
                }
            };
            if let Err(error) = std::fs::create_dir_all(parent) {
                return Err(error);
            }
            if let Err(error) = std::fs::copy(file.path(), &path) {
                return Err(error);
            }
            if let Err(error) = std::fs::remove_file(file.path()) {
                return Err(error);
            }
            file.set_path(path);
            new_files.push(file);
        }

        let backup: Snapshot<Structured> = Snapshot {
            state: std::marker::PhantomData,
            commit_hash: self.commit_hash,
            files: new_files,
            tmp_dir: self.tmp_dir,
        };

        Ok(backup)
    }
}

impl Snapshot<Structured> {
    pub fn flatten_directory_structure(self) -> Result<Snapshot<Flattened>, std::io::Error> {
        unimplemented!("Not yet implemented")
    }
}

impl<State> Snapshot<State> {
    pub fn len(&self) -> usize {
        self.files.len()
    }

    pub fn size(&self) -> u64 {
        self.files.iter().map(|file| file.size()).sum::<u64>()
    }

    pub fn is_empty(&self) -> bool {
        self.files.is_empty()
    }

    pub fn commit_hash_value(&self) -> &str {
        self.commit_hash.hash()
    }

    pub fn tmp_dir(&self) -> &PathBuf {
        self.tmp_dir.path()
    }

    pub fn files(&self) -> &[File] {
        &self.files
    }

    pub fn all_file_paths(&self) -> Vec<&Path> {
        let mut file_paths: Vec<&Path> = Vec::new();
        for file in &self.files {
            file_paths.push(file.path());
        }
        file_paths
    }
}
