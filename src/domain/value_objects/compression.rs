#[derive(Debug, Clone, PartialEq)]
pub enum Compression {
    Gzip,
    None,
}

impl From<&[u8]> for Compression {
    // uses magic numbers (https://en.wikipedia.org/wiki/List_of_file_signatures)
    fn from(byte_slice: &[u8]) -> Self {
        match byte_slice {
            [0x1f, 0x8b, ..] => Compression::Gzip,
            _ => Compression::None,
        }
    }
}
