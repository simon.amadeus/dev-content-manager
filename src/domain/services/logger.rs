pub trait Logger {
    fn trace(&self, target: &str, message: &str);
    fn debug(&self, target: &str, message: &str);
    fn info(&self, target: &str, message: &str);
    fn warn(&self, target: &str, message: &str);
    fn error(&self, target: &str, message: &str);
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use std::cell::RefCell;

    #[derive(Debug, Clone)]
    pub struct MockLogger {
        trace_messages: RefCell<Vec<String>>,
        debug_messages: RefCell<Vec<String>>,
        info_messages: RefCell<Vec<String>>,
        warn_messages: RefCell<Vec<String>>,
        error_messages: RefCell<Vec<String>>,
    }

    impl MockLogger {
        pub fn new() -> Self {
            Self {
                trace_messages: RefCell::new(vec![]),
                debug_messages: RefCell::new(vec![]),
                info_messages: RefCell::new(vec![]),
                warn_messages: RefCell::new(vec![]),
                error_messages: RefCell::new(vec![]),
            }
        }

        pub fn trace_messages(&self) -> Vec<String> {
            self.trace_messages.borrow().clone()
        }

        pub fn debug_messages(&self) -> Vec<String> {
            self.debug_messages.borrow().clone()
        }

        pub fn info_messages(&self) -> Vec<String> {
            self.info_messages.borrow().clone()
        }

        pub fn warn_messages(&self) -> Vec<String> {
            self.warn_messages.borrow().clone()
        }

        pub fn error_messages(&self) -> Vec<String> {
            self.error_messages.borrow().clone()
        }

        pub fn trace_was_called(&self) -> bool {
            !self.trace_messages.borrow().is_empty()
        }

        pub fn debug_was_called(&self) -> bool {
            !self.debug_messages.borrow().is_empty()
        }

        pub fn info_was_called(&self) -> bool {
            !self.info_messages.borrow().is_empty()
        }

        pub fn warn_was_called(&self) -> bool {
            !self.warn_messages.borrow().is_empty()
        }

        pub fn error_was_called(&self) -> bool {
            !self.error_messages.borrow().is_empty()
        }
    }

    impl Logger for MockLogger {
        fn trace(&self, _target: &str, message: &str) {
            self.trace_messages.borrow_mut().push(message.to_string())
        }

        fn debug(&self, _target: &str, message: &str) {
            self.debug_messages.borrow_mut().push(message.to_string())
        }

        fn info(&self, _target: &str, message: &str) {
            self.info_messages.borrow_mut().push(message.to_string())
        }

        fn warn(&self, _target: &str, message: &str) {
            self.warn_messages.borrow_mut().push(message.to_string())
        }

        fn error(&self, _target: &str, message: &str) {
            self.error_messages.borrow_mut().push(message.to_string())
        }
    }
}
