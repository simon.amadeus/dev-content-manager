use crate::domain::entities::file::{File, FileKind};
use std::path::Path;

pub trait FileFactory {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn create_file_from_path<P: AsRef<Path>>(
        &self,
        path: P,
        kind: FileKind,
    ) -> Result<File, Self::Error>;
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use std::cell::RefCell;

    #[derive(Debug, Clone)]
    pub struct MockFileFactory {
        success: bool,
        called_create_file_from_path_count: RefCell<u8>,
        content_file: Option<File>,
        database_structure_dump_file: Option<File>,
        database_content_dump_file: Option<File>,
    }

    impl MockFileFactory {
        pub fn new(
            success: bool,
            content_file: Option<File>,
            database_structure_dump_file: Option<File>,
            database_content_dump_file: Option<File>,
        ) -> Self {
            Self {
                success,
                called_create_file_from_path_count: RefCell::new(0),
                content_file,
                database_structure_dump_file,
                database_content_dump_file,
            }
        }

        pub fn called_create_file_from_path_count(&self) -> u8 {
            *self.called_create_file_from_path_count.borrow()
        }

        pub fn create_file_from_path_was_called(&self) -> bool {
            self.called_create_file_from_path_count() > 0
        }
    }

    impl FileFactory for MockFileFactory {
        type Error = MockFileFactoryError;

        fn create_file_from_path<P: AsRef<Path>>(
            &self,
            _path: P,
            kind: FileKind,
        ) -> Result<File, Self::Error> {
            *self.called_create_file_from_path_count.borrow_mut() += 1;

            if self.success {
                match kind {
                    FileKind::ContentFile => {
                        if let Some(file) = &self.content_file {
                            Ok(file.clone())
                        } else {
                            Err(MockFileFactoryError::LoadFileError)
                        }
                    }
                    FileKind::DatabaseStructureDump => {
                        if let Some(file) = &self.database_structure_dump_file {
                            Ok(file.clone())
                        } else {
                            Err(MockFileFactoryError::LoadFileError)
                        }
                    }
                    FileKind::DatabaseContentDump => {
                        if let Some(file) = &self.database_content_dump_file {
                            Ok(file.clone())
                        } else {
                            Err(MockFileFactoryError::LoadFileError)
                        }
                    }
                }
            } else {
                Err(MockFileFactoryError::LoadFileError)
            }
        }
    }

    #[derive(Debug)]
    pub enum MockFileFactoryError {
        LoadFileError,
    }

    impl std::error::Error for MockFileFactoryError {}

    impl std::fmt::Display for MockFileFactoryError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "MockFileFactoryError")
        }
    }

    pub struct MockFileFactoryBuilder {
        success: bool,
        content_file: Option<File>,
        database_structure_dump_file: Option<File>,
        database_content_dump_file: Option<File>,
    }

    impl MockFileFactoryBuilder {
        pub fn new() -> Self {
            Self {
                success: true,
                content_file: None,
                database_structure_dump_file: None,
                database_content_dump_file: None,
            }
        }

        pub fn with_error(mut self) -> Self {
            self.success = false;
            self
        }

        pub fn with_file(mut self, file: File) -> Self {
            match file.kind() {
                FileKind::ContentFile => self.content_file = Some(file),
                FileKind::DatabaseStructureDump => self.database_structure_dump_file = Some(file),
                FileKind::DatabaseContentDump => self.database_content_dump_file = Some(file),
            }

            self
        }

        pub fn build(self) -> MockFileFactory {
            MockFileFactory::new(
                self.success,
                self.content_file,
                self.database_structure_dump_file,
                self.database_content_dump_file,
            )
        }
    }
}
