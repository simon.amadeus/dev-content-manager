use std::path::{Path, PathBuf};

pub trait FileCompressor {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn compress_file<P: AsRef<Path>>(&self, file: P, target_dir: P)
        -> Result<PathBuf, Self::Error>;
    fn decompress_file<P: AsRef<Path>>(&self, file: P) -> Result<PathBuf, Self::Error>;
    fn pack_files<P: AsRef<Path>>(
        &self,
        files: &[P],
        target_dir: P,
    ) -> Result<PathBuf, Self::Error>;
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use std::{cell::RefCell, path::PathBuf};

    #[derive(Debug, Clone, Default)]
    pub struct MockFileCompressor {
        success: bool,
        called_compress_file_count: RefCell<u8>,
        called_decompress_file_count: RefCell<u8>,
        called_pack_files_count: RefCell<u8>,
        input_file: RefCell<Option<PathBuf>>,
        output_file: RefCell<Option<PathBuf>>,
        output_dir: RefCell<Option<PathBuf>>,
    }

    impl MockFileCompressor {
        pub fn new(success: bool, output_file: Option<PathBuf>) -> Self {
            Self {
                success,
                called_compress_file_count: RefCell::new(0),
                called_decompress_file_count: RefCell::new(0),
                called_pack_files_count: RefCell::new(0),
                input_file: RefCell::new(None),
                output_file: RefCell::new(output_file),
                output_dir: RefCell::new(None),
            }
        }

        pub fn compress_file_was_called(&self) -> bool {
            *self.called_compress_file_count.borrow() > 0
        }

        pub fn decompress_file_was_called(&self) -> bool {
            *self.called_decompress_file_count.borrow() > 0
        }

        pub fn pack_files_was_called(&self) -> bool {
            *self.called_pack_files_count.borrow() > 0
        }

        pub fn called_compress_file_count(&self) -> u8 {
            *self.called_compress_file_count.borrow()
        }

        pub fn called_decompress_file_count(&self) -> u8 {
            *self.called_decompress_file_count.borrow()
        }

        pub fn called_pack_files_count(&self) -> u8 {
            *self.called_pack_files_count.borrow()
        }

        pub fn get_input_file(&self) -> Option<PathBuf> {
            self.input_file.borrow().clone()
        }

        pub fn get_output_file(&self) -> Option<PathBuf> {
            self.output_file.borrow().clone()
        }

        pub fn get_output_dir(&self) -> Option<PathBuf> {
            self.output_dir.borrow().clone()
        }
    }

    impl FileCompressor for MockFileCompressor {
        type Error = MockFileCompressorError;

        fn compress_file<P: AsRef<Path>>(
            &self,
            file: P,
            target_dir: P,
        ) -> Result<PathBuf, Self::Error> {
            *self.called_compress_file_count.borrow_mut() += 1;
            *self.input_file.borrow_mut() = Some(file.as_ref().to_path_buf());
            *self.output_dir.borrow_mut() = Some(target_dir.as_ref().to_path_buf());
            if self.success {
                Ok(self
                    .output_file
                    .borrow()
                    .clone()
                    .expect("No output file provided"))
            } else {
                Err(MockFileCompressorError::CompressFileError)
            }
        }

        fn decompress_file<P: AsRef<Path>>(&self, file: P) -> Result<PathBuf, Self::Error> {
            *self.called_decompress_file_count.borrow_mut() += 1;
            *self.input_file.borrow_mut() = Some(file.as_ref().to_path_buf());
            if self.success {
                Ok(self
                    .output_file
                    .borrow()
                    .clone()
                    .expect("No output file provided"))
            } else {
                Err(MockFileCompressorError::DecompressFileError)
            }
        }

        fn pack_files<P: AsRef<Path>>(
            &self,
            files: &[P],
            target_dir: P,
        ) -> Result<PathBuf, Self::Error> {
            *self.called_pack_files_count.borrow_mut() += 1;
            *self.input_file.borrow_mut() = Some(files[0].as_ref().to_path_buf());
            *self.output_dir.borrow_mut() = Some(target_dir.as_ref().to_path_buf());
            if self.success {
                Ok(self
                    .output_file
                    .borrow()
                    .clone()
                    .expect("No output file provided"))
            } else {
                Err(MockFileCompressorError::PackFilesError)
            }
        }
    }

    #[derive(Debug)]
    pub enum MockFileCompressorError {
        CompressFileError,
        DecompressFileError,
        PackFilesError,
    }

    impl std::error::Error for MockFileCompressorError {}

    impl std::fmt::Display for MockFileCompressorError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                MockFileCompressorError::CompressFileError => {
                    write!(f, "CompressFileError")
                }
                MockFileCompressorError::DecompressFileError => {
                    write!(f, "DecompressFileError")
                }
                MockFileCompressorError::PackFilesError => {
                    write!(f, "PackFilesError")
                }
            }
        }
    }

    pub struct MockFileCompressorBuilder {
        success: bool,
        output_file: Option<PathBuf>,
    }

    impl MockFileCompressorBuilder {
        pub fn new() -> Self {
            Self {
                success: true,
                output_file: None,
            }
        }

        pub fn with_error(mut self) -> Self {
            self.success = false;
            self
        }

        pub fn with_output_file(mut self, output_file: PathBuf) -> Self {
            self.output_file = Some(output_file);
            self
        }

        pub fn build(self) -> MockFileCompressor {
            MockFileCompressor::new(self.success, self.output_file)
        }
    }
}
