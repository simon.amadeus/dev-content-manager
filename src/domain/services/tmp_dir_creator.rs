use std::path::PathBuf;

pub trait TmpDirCreator {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn create_tmp_dir(&self, name: Option<String>) -> Result<PathBuf, Self::Error>;
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use std::{cell::RefCell, path::PathBuf};

    #[derive(Debug, Clone, Default)]
    pub struct MockTmpDirCreator {
        success: bool,
        called_create_tmp_dir_count: RefCell<u8>,
        tmp_dir: RefCell<Option<PathBuf>>,
    }

    impl MockTmpDirCreator {
        pub fn new(success: bool, tmp_dir: Option<PathBuf>) -> Self {
            Self {
                success,
                called_create_tmp_dir_count: RefCell::new(0),
                tmp_dir: RefCell::new(tmp_dir),
            }
        }

        pub fn create_tmp_dir_was_called(&self) -> bool {
            *self.called_create_tmp_dir_count.borrow() > 0
        }

        pub fn called_create_tmp_dir_count(&self) -> u8 {
            *self.called_create_tmp_dir_count.borrow()
        }

        pub fn get_tmp_dir(&self) -> Option<PathBuf> {
            self.tmp_dir.borrow().clone()
        }
    }

    impl TmpDirCreator for MockTmpDirCreator {
        type Error = MockTmpDirCreatorError;

        fn create_tmp_dir(&self, _name: Option<String>) -> Result<PathBuf, Self::Error> {
            *self.called_create_tmp_dir_count.borrow_mut() += 1;

            if self.success {
                let tmp_dir = self.tmp_dir.borrow().clone().expect("no tmp dir provided");
                Ok(tmp_dir)
            } else {
                Err(MockTmpDirCreatorError::CreateTmpDirError)
            }
        }
    }

    #[derive(Debug)]
    pub enum MockTmpDirCreatorError {
        CreateTmpDirError,
    }

    impl std::fmt::Display for MockTmpDirCreatorError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                MockTmpDirCreatorError::CreateTmpDirError => {
                    write!(f, "CreateTmpDirError")
                }
            }
        }
    }

    impl std::error::Error for MockTmpDirCreatorError {}

    #[derive(Debug)]
    pub struct MockTmpDirCreatorBuilder {
        success: bool,
        tmp_dir: Option<PathBuf>,
    }

    impl MockTmpDirCreatorBuilder {
        pub fn new() -> Self {
            Self {
                success: true,
                tmp_dir: None,
            }
        }

        pub fn with_error(mut self) -> Self {
            self.success = false;
            self
        }

        pub fn with_tmp_dir(mut self, tmp_dir: PathBuf) -> Self {
            self.tmp_dir = Some(tmp_dir);
            self
        }

        pub fn build(self) -> MockTmpDirCreator {
            MockTmpDirCreator::new(self.success, self.tmp_dir)
        }
    }
}
