use std::path::{Path, PathBuf};

pub trait DatabaseDumper {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn export_structure_dump<P: AsRef<Path>>(&self, output_dir: P) -> Result<PathBuf, Self::Error>;
    fn export_content_dump<P: AsRef<Path>>(&self, output_dir: P) -> Result<PathBuf, Self::Error>;
    fn import_database_dump<P: AsRef<Path>>(&self, file_path: P) -> Result<(), Self::Error>;
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use std::{cell::RefCell, path::PathBuf};

    #[derive(Debug, Clone)]
    pub struct MockDatabaseDumper {
        success: bool,
        mocked_structure_dump: RefCell<PathBuf>,
        mocked_content_dump: RefCell<PathBuf>,
        output_dir: RefCell<Option<PathBuf>>,
        import_file: RefCell<Option<PathBuf>>,
        called_export_structure_dump_count: RefCell<u8>,
        called_export_content_dump_count: RefCell<u8>,
        called_import_database_dump_count: RefCell<u8>,
    }

    impl MockDatabaseDumper {
        pub fn new(
            success: bool,
            mocked_structure_dump: PathBuf,
            mocked_content_dump: PathBuf,
        ) -> Self {
            Self {
                success,
                mocked_structure_dump: RefCell::new(mocked_structure_dump),
                mocked_content_dump: RefCell::new(mocked_content_dump),
                output_dir: RefCell::new(None),
                import_file: RefCell::new(None),
                called_export_structure_dump_count: RefCell::new(0),
                called_export_content_dump_count: RefCell::new(0),
                called_import_database_dump_count: RefCell::new(0),
            }
        }

        pub fn get_output_dir(&self) -> Option<PathBuf> {
            self.output_dir.borrow().clone()
        }

        pub fn get_import_file(&self) -> Option<PathBuf> {
            self.import_file.borrow().clone()
        }

        pub fn export_structure_dump_was_called(&self) -> bool {
            *self.called_export_structure_dump_count.borrow() > 0
        }

        pub fn export_content_dump_was_called(&self) -> bool {
            *self.called_export_content_dump_count.borrow() > 0
        }

        pub fn import_database_dump_was_called(&self) -> bool {
            *self.called_import_database_dump_count.borrow() > 0
        }

        pub fn called_export_structure_dump_count(&self) -> u8 {
            *self.called_export_structure_dump_count.borrow()
        }

        pub fn called_export_content_dump_count(&self) -> u8 {
            *self.called_export_content_dump_count.borrow()
        }

        pub fn called_import_database_dump_count(&self) -> u8 {
            *self.called_import_database_dump_count.borrow()
        }
    }

    impl DatabaseDumper for MockDatabaseDumper {
        type Error = MockDatabaseDumperError;

        fn export_structure_dump<P: AsRef<Path>>(
            &self,
            output_dir: P,
        ) -> Result<PathBuf, Self::Error> {
            *self.called_export_structure_dump_count.borrow_mut() += 1;
            *self.output_dir.borrow_mut() = Some(output_dir.as_ref().to_path_buf());
            if self.success {
                Ok(self.mocked_structure_dump.borrow().clone())
            } else {
                Err(MockDatabaseDumperError::ExportError)
            }
        }

        fn export_content_dump<P: AsRef<Path>>(
            &self,
            output_dir: P,
        ) -> Result<PathBuf, Self::Error> {
            *self.called_export_content_dump_count.borrow_mut() += 1;
            *self.output_dir.borrow_mut() = Some(output_dir.as_ref().to_path_buf());
            if self.success {
                Ok(self.mocked_content_dump.borrow().clone())
            } else {
                Err(MockDatabaseDumperError::ExportError)
            }
        }

        fn import_database_dump<P: AsRef<Path>>(&self, file_path: P) -> Result<(), Self::Error> {
            *self.called_import_database_dump_count.borrow_mut() += 1;
            *self.import_file.borrow_mut() = Some(file_path.as_ref().to_path_buf());
            if self.success {
                Ok(())
            } else {
                Err(MockDatabaseDumperError::ImportError)
            }
        }
    }

    #[derive(Debug)]
    pub enum MockDatabaseDumperError {
        ExportError,
        ImportError,
    }

    impl std::error::Error for MockDatabaseDumperError {}

    impl std::fmt::Display for MockDatabaseDumperError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                MockDatabaseDumperError::ExportError => write!(f, "ExportError"),
                MockDatabaseDumperError::ImportError => write!(f, "ImportError"),
            }
        }
    }

    pub struct MockDatabaseDumperBuilder {
        success: bool,
        mocked_structure_dump: Option<PathBuf>,
        mocked_content_dump: Option<PathBuf>,
    }

    impl MockDatabaseDumperBuilder {
        pub fn new() -> Self {
            Self {
                success: true,
                mocked_structure_dump: None,
                mocked_content_dump: None,
            }
        }

        pub fn with_error(mut self) -> Self {
            self.success = false;
            self
        }

        pub fn with_mocked_structure_dump(mut self, mocked_structure_dump: PathBuf) -> Self {
            self.mocked_structure_dump = Some(mocked_structure_dump);
            self
        }

        pub fn with_mocked_content_dump(mut self, mocked_content_dump: PathBuf) -> Self {
            self.mocked_content_dump = Some(mocked_content_dump);
            self
        }

        pub fn build(self) -> MockDatabaseDumper {
            MockDatabaseDumper::new(
                self.success,
                self.mocked_structure_dump.unwrap_or_default(),
                self.mocked_content_dump.unwrap_or_default(),
            )
        }
    }
}
