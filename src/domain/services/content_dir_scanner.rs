use std::path::PathBuf;

pub trait ContentDirScanner {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn scan_content_dir(&self) -> Result<Vec<PathBuf>, Self::Error>;
    // TODO: copy files to content dir
}
// TODO: rename content dir handler

#[cfg(test)]
pub mod tests {
    use super::*;
    use std::cell::RefCell;

    #[derive(Debug)]
    pub struct MockContentDirScanner {
        success: bool,
        files: Option<Vec<PathBuf>>,
        called_scan_content_dir_count: RefCell<u8>,
    }

    impl MockContentDirScanner {
        pub fn new(success: bool, files: Option<Vec<PathBuf>>) -> Self {
            Self {
                success,
                files,
                called_scan_content_dir_count: RefCell::new(0),
            }
        }

        pub fn files(&self) -> Option<&Vec<PathBuf>> {
            self.files.as_ref()
        }

        pub fn scan_content_dir_was_called(&self) -> bool {
            *self.called_scan_content_dir_count.borrow() > 0
        }

        pub fn called_scan_content_dir_count(&self) -> u8 {
            *self.called_scan_content_dir_count.borrow()
        }
    }

    impl ContentDirScanner for MockContentDirScanner {
        type Error = MockContentDirScannerError;

        fn scan_content_dir(&self) -> Result<Vec<PathBuf>, Self::Error> {
            *self.called_scan_content_dir_count.borrow_mut() += 1;

            if self.success {
                Ok(self.files.clone().expect("files is None"))
            } else {
                Err(MockContentDirScannerError::ScanContentDirError)
            }
        }
    }

    #[derive(Debug)]
    pub enum MockContentDirScannerError {
        ScanContentDirError,
    }

    impl std::error::Error for MockContentDirScannerError {}

    impl std::fmt::Display for MockContentDirScannerError {
        fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
            match self {
                MockContentDirScannerError::ScanContentDirError => {
                    write!(f, "ScanContentDirError")
                }
            }
        }
    }

    pub struct MockContentDirScannerBuilder {
        success: bool,
        files: Option<Vec<PathBuf>>,
    }

    impl MockContentDirScannerBuilder {
        pub fn new() -> Self {
            Self {
                success: true,
                files: None,
            }
        }

        pub fn with_error(mut self) -> Self {
            self.success = false;
            self
        }

        pub fn with_files(mut self, files: Vec<PathBuf>) -> Self {
            self.files = Some(files);
            self
        }

        pub fn build(self) -> MockContentDirScanner {
            MockContentDirScanner::new(self.success, self.files)
        }
    }
}
