use crate::domain::services::tmp_dir_creator::TmpDirCreator;
use std::path::PathBuf;
use uuid::Uuid;

#[derive(Debug, Clone)]
pub struct TmpDirCreatorImpl {
    base_dir: PathBuf,
}

impl TmpDirCreatorImpl {
    pub fn new(base_dir: PathBuf) -> Self {
        TmpDirCreatorImpl { base_dir }
    }
}

impl TmpDirCreator for TmpDirCreatorImpl {
    type Error = TmpDirCreatorImplError;

    fn create_tmp_dir(&self, name: Option<String>) -> Result<std::path::PathBuf, Self::Error> {
        let dir_name = match name {
            Some(name) => name,
            None => Uuid::new_v4().to_string(),
        };
        let path = self.base_dir.join(dir_name);

        std::fs::create_dir(&path)?;

        Ok(path)
    }
}

#[derive(Debug)]
pub enum TmpDirCreatorImplError {
    IoError(std::io::Error),
}

impl std::fmt::Display for TmpDirCreatorImplError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            TmpDirCreatorImplError::IoError(e) => write!(f, "{}", e),
        }
    }
}

impl std::error::Error for TmpDirCreatorImplError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            TmpDirCreatorImplError::IoError(e) => Some(e),
        }
    }
}

impl From<std::io::Error> for TmpDirCreatorImplError {
    fn from(e: std::io::Error) -> Self {
        TmpDirCreatorImplError::IoError(e)
    }
}
