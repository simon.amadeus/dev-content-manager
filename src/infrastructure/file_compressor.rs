use crate::domain::services::file_compressor::FileCompressor;
use flate2::write::GzEncoder;
use flate2::Compression as FlateCompression;
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;
use std::time::SystemTime;

#[derive(Debug, Clone)]
pub struct GzipCompressor;

impl GzipCompressor {
    pub fn new() -> Self {
        Self
    }
}

impl FileCompressor for GzipCompressor {
    type Error = FileCompressorError;

    fn compress_file<P: AsRef<Path>>(
        &self,
        source: P,
        target_dir: P,
    ) -> Result<PathBuf, Self::Error> {
        let source_name: String = match source.as_ref().file_name() {
            Some(name) => name
                .to_str()
                .ok_or_else(|| {
                    FileCompressorError::IoError(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        "file name is not valid UTF-8",
                    ))
                })?
                .to_string(),
            None => {
                return Err(FileCompressorError::IoError(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "file name is not valid UTF-8",
                )))
            }
        };
        let target_name = format!("{}.gz", &source_name);
        let target_path = target_dir.as_ref().join(&target_name);

        let byte_content = std::fs::read(source).map_err(FileCompressorError::IoError)?;

        let mut encoder = GzEncoder::new(Vec::new(), FlateCompression::default());
        encoder
            .write_all(&byte_content)
            .map_err(FileCompressorError::IoError)?;

        let target_content = encoder.finish()?;
        std::fs::write(&target_path, target_content).map_err(FileCompressorError::IoError)?;

        Ok(target_path)
    }

    fn decompress_file<P: AsRef<Path>>(&self, _file: P) -> Result<PathBuf, Self::Error> {
        unimplemented!()
    }

    fn pack_files<P: AsRef<Path>>(
        &self,
        files: &[P],
        target_dir: P,
    ) -> Result<PathBuf, Self::Error> {
        let package_file_name = format!(
            "content_package_{}.tar.gz",
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)?
                .as_secs()
        );
        let package_file_path = target_dir.as_ref().join(&package_file_name);
        let enc = GzEncoder::new(
            std::fs::File::create(&package_file_path)?,
            FlateCompression::default(),
        );

        let mut tar = tar::Builder::new(enc);

        let pb = indicatif::ProgressBar::new(files.len() as u64);
        for file in files {
            pb.inc(1);
            tar.append_path(file)?;
        }
        pb.finish_and_clear();

        let _enc = tar.into_inner()?;

        Ok(package_file_path)
    }
}

#[derive(Debug)]
pub enum FileCompressorError {
    IoError(std::io::Error),
    SystemTimeError(std::time::SystemTimeError),
}

impl std::fmt::Display for FileCompressorError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            FileCompressorError::IoError(e) => write!(f, "{}", e),
            FileCompressorError::SystemTimeError(e) => write!(f, "{}", e),
        }
    }
}

impl std::error::Error for FileCompressorError {}

impl From<std::io::Error> for FileCompressorError {
    fn from(e: std::io::Error) -> Self {
        FileCompressorError::IoError(e)
    }
}

impl From<std::time::SystemTimeError> for FileCompressorError {
    fn from(e: std::time::SystemTimeError) -> Self {
        FileCompressorError::SystemTimeError(e)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::domain::services::file_compressor::FileCompressor;
    use std::path::PathBuf;
    use uuid::Uuid;

    fn get_tmp_dir() -> PathBuf {
        let name = Uuid::new_v4().to_string();
        let tmp_parent_dir = std::env::temp_dir();
        let tmp_dir = tmp_parent_dir.join("file_compressor_test");
        let tmp_file_path = tmp_dir.join(&name);

        std::fs::create_dir_all(&tmp_dir).expect("failed to create tmp dir");
        std::fs::write(&tmp_file_path, "test").expect("failed to create tmp file with content");

        tmp_dir
    }

    fn get_uncompressed_sql_file() -> PathBuf {
        let name = format!("{}.sql", Uuid::new_v4().to_string());
        let tmp_parent_dir = std::env::temp_dir();
        let tmp_dir = tmp_parent_dir.join("file_compressor_test");
        let tmp_file_path = tmp_dir.join(&name);

        std::fs::create_dir_all(&tmp_dir).expect("failed to create tmp dir");
        std::fs::write(&tmp_file_path, "test").expect("failed to create tmp file with content");

        tmp_file_path
    }

    #[test]
    fn compress_file_returns_file_with_correct_extension() {
        let tmp_dir = get_tmp_dir();
        let file = get_uncompressed_sql_file();
        let expected_extension = "gz";

        let compressed_file = GzipCompressor
            .compress_file(&file, &tmp_dir)
            .expect("failed to compress file");

        let received_extension = compressed_file
            .extension()
            .expect("failed to get extension")
            .to_str()
            .expect("failed to convert extension to str");

        assert_eq!(expected_extension, received_extension);
    }

    #[test]
    fn pack_files_returns_file_with_correct_suffix() {
        // the source dir is used to copy the file to a new location
        // because tar requires the file path to be relative
        let random_name = Uuid::new_v4().to_string();
        let source_dir = PathBuf::from(".").join(&random_name);
        std::fs::create_dir_all(&source_dir).expect("failed to create tmp dir");
        let target_dir = get_tmp_dir();
        let file = get_uncompressed_sql_file();
        let source_file = source_dir.join(file.file_name().expect("failed to get file name"));
        std::fs::copy(&file, &source_file).expect("failed to copy file to source dir");
        let expected_suffix = "tar.gz";

        let compressed_file = GzipCompressor
            .pack_files(&[&source_file], &target_dir)
            .expect("failed to compress file");
        std::fs::remove_dir_all(&source_dir).expect("failed to remove tmp dir");

        let received_name = compressed_file
            .file_name()
            .expect("failed to get file name")
            .to_str()
            .expect("failed to convert file name to str");

        assert!(received_name.ends_with(expected_suffix));
    }
}
