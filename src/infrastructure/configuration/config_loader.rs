use std::path::PathBuf;

use super::AppConfig;

pub struct ConfigLoader {
    config_file_path: PathBuf,
}

impl ConfigLoader {
    pub fn new(config_file_path: PathBuf) -> Self {
        ConfigLoader { config_file_path }
    }

    pub fn load(&self) -> Result<AppConfig, ConfigLoaderError> {
        let config_file_content = std::fs::read_to_string(&self.config_file_path)?;
        let app_config: AppConfig = serde_json::from_str(&config_file_content)?;

        Ok(app_config)
    }

    pub fn save(&self, app_config: &AppConfig) -> Result<(), ConfigLoaderError> {
        let config_file_content = serde_json::to_string_pretty(app_config)?;

        std::fs::write(&self.config_file_path, config_file_content)?;

        Ok(())
    }
}

#[derive(Debug)]
pub enum ConfigLoaderError {
    IoError(std::io::Error),
    ParseError(serde_json::Error),
}

impl From<std::io::Error> for ConfigLoaderError {
    fn from(error: std::io::Error) -> Self {
        ConfigLoaderError::IoError(error)
    }
}

impl From<serde_json::Error> for ConfigLoaderError {
    fn from(error: serde_json::Error) -> Self {
        ConfigLoaderError::ParseError(error)
    }
}
