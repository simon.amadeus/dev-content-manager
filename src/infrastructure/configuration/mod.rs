mod config_loader;
pub use config_loader::ConfigLoader;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AppConfig {
    pub relative_path_to_content_root: String,
    pub database: DatabaseConfig,
    pub holochain: HolochainConfig,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct DatabaseConfig {
    pub host: String,
    pub port: u16,
    pub user: String,
    pub password: String,
    pub database: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct HolochainConfig {
    pub keystore_url: String,
    pub keystore_passphrase: String,
    pub happ_websocket_url: String,
}
