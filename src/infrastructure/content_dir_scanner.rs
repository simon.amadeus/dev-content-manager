use crate::domain::services::content_dir_scanner::ContentDirScanner;
use serde::{Deserialize, Serialize};
use std::{
    fmt::{Debug, Display},
    path::PathBuf,
};
use walkdir::WalkDir;

#[derive(Clone, Debug)]
pub struct ContentDirScannerImpl {
    config: ContentDirScannerImplConfig,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct ContentDirScannerImplConfig {
    pub content_dir: PathBuf,
}

impl ContentDirScannerImplConfig {
    pub fn new(content_dir: PathBuf) -> Self {
        Self { content_dir }
    }
}

impl ContentDirScannerImpl {
    pub fn new(config: ContentDirScannerImplConfig) -> Self {
        Self { config }
    }
}

impl ContentDirScanner for ContentDirScannerImpl {
    type Error = ContentDirScannerImplError;

    fn scan_content_dir(&self) -> Result<Vec<PathBuf>, Self::Error> {
        let walker = WalkDir::new(&self.config.content_dir).into_iter();
        let mut files: Vec<PathBuf> = Vec::new();

        for entry in walker {
            let entry = entry?;
            if entry.file_type().is_file() {
                let file_path = entry.path().to_path_buf();
                files.push(file_path);
            }
        }

        files.sort();
        files.dedup();

        Ok(files)
    }
}

#[derive(Debug)]
pub enum ContentDirScannerImplError {
    IoError(std::io::Error),
    WalkDirError(walkdir::Error),
    NoContentDirFound,
    InvalidFilePath(String),
    SharedStateError(String),
}

impl Display for ContentDirScannerImplError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ContentDirScannerImplError::IoError(e) => write!(f, "IO Error: {}", e),
            ContentDirScannerImplError::NoContentDirFound => {
                write!(f, "No content folder found")
            }
            ContentDirScannerImplError::InvalidFilePath(path) => {
                write!(f, "Invalid file path: {}", path)
            }
            ContentDirScannerImplError::WalkDirError(e) => {
                write!(f, "WalkDir Error: {}", e)
            }
            ContentDirScannerImplError::SharedStateError(e) => {
                write!(f, "Shared state error: {}", e)
            }
        }
    }
}

impl std::error::Error for ContentDirScannerImplError {}

impl From<std::io::Error> for ContentDirScannerImplError {
    fn from(e: std::io::Error) -> Self {
        ContentDirScannerImplError::IoError(e)
    }
}

impl From<walkdir::Error> for ContentDirScannerImplError {
    fn from(e: walkdir::Error) -> Self {
        ContentDirScannerImplError::WalkDirError(e)
    }
}
