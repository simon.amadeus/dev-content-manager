use crate::domain::services::database_dumper::DatabaseDumper;
use crate::infrastructure::configuration::DatabaseConfig;
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};
use std::process::Command;

#[derive(Debug, Clone)]
pub struct MySqlDumper {
    config: MySqlConfig,
}

impl MySqlDumper {
    pub fn new(config: MySqlConfig) -> Self {
        MySqlDumper { config }
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct MySqlConfig {
    pub host: String,
    pub port: u16,
    pub user: String,
    pub password: String,
    pub database: String,
}

impl MySqlConfig {
    pub fn mysql_connection_string(&self) -> String {
        format!(
            "mysql://{}:{}@{}:{}/{}",
            self.user, self.password, self.host, self.port, self.database
        )
    }
}

impl From<DatabaseConfig> for MySqlConfig {
    fn from(config: DatabaseConfig) -> Self {
        Self {
            host: config.host,
            port: config.port,
            user: config.user,
            password: config.password,
            database: config.database,
        }
    }
}

impl DatabaseDumper for MySqlDumper {
    type Error = MySqlDumperError;

    fn export_structure_dump<P: AsRef<Path>>(&self, target_dir: P) -> Result<PathBuf, Self::Error> {
        let target_name = format!("{}_structure.sql", self.config.database);
        let target_path = target_dir.as_ref().join(target_name);
        // export password variable
        let status = Command::new("mysqldump")
            .env("MYSQL_PWD", &self.config.password)
            .arg("--column-statistics=0")
            .arg("-h") // host
            .arg(&self.config.host)
            .arg("-P") // port
            .arg(&self.config.port.to_string())
            .arg("-u") // username
            .arg(&self.config.user)
            .arg("--no-data") // no data
            .arg("-r") // result file
            .arg(&target_path)
            .arg(&self.config.database)
            .status()?;

        match status.code() {
            Some(0) => Ok(target_path),
            _ => Err(MySqlDumperError::ExportFailed),
        }
    }

    fn export_content_dump<P: AsRef<Path>>(&self, output_dir: P) -> Result<PathBuf, Self::Error> {
        let target_name = format!("{}_content.sql", self.config.database);
        let target_path = output_dir.as_ref().join(target_name);
        let status = Command::new("mysqldump")
            .env("MYSQL_PWD", &self.config.password)
            .arg("--column-statistics=0")
            .arg("-h") // host
            .arg(&self.config.host)
            .arg("-P") // port
            .arg(&self.config.port.to_string())
            .arg("-u") // username
            .arg(&self.config.user)
            .arg("--no-create-info") // no create table statements
            .arg("-r") // result file
            .arg(&target_path)
            .arg(&self.config.database)
            .status()?;

        match status.code() {
            Some(0) => Ok(target_path),
            _ => Err(MySqlDumperError::ExportFailed),
        }
    }

    fn import_database_dump<P: AsRef<Path>>(&self, file_path: P) -> Result<(), Self::Error> {
        let status = Command::new("mysql")
            .arg("-h") // host
            .arg(&self.config.host)
            .arg("-P") // port
            .arg(&self.config.port.to_string())
            .arg("-u") // username
            .arg(&self.config.user)
            .arg("-p") // password
            .arg(&self.config.password)
            .arg(&self.config.database)
            .arg("<") // pipe the input file
            .arg(file_path.as_ref())
            .status()?;

        match status.code() {
            Some(0) => Ok(()),
            _ => Err(MySqlDumperError::ImportFailed),
        }
    }
}

#[derive(Debug)]
pub enum MySqlDumperError {
    ExportFailed,
    ImportFailed,
    IoError(std::io::Error),
    SharedStateError(String),
}

impl From<std::io::Error> for MySqlDumperError {
    fn from(error: std::io::Error) -> Self {
        MySqlDumperError::IoError(error)
    }
}

impl std::fmt::Display for MySqlDumperError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MySqlDumperError::ExportFailed => write!(f, "Export failed"),
            MySqlDumperError::ImportFailed => write!(f, "Import failed"),
            MySqlDumperError::IoError(e) => write!(f, "IO error: {}", e),
            MySqlDumperError::SharedStateError(e) => write!(f, "Shared state error: {}", e),
        }
    }
}

impl std::error::Error for MySqlDumperError {}
