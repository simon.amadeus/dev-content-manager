use crate::domain::services::logger::Logger;

#[derive(Debug, Clone)]
pub struct LogCrateLogger;

impl LogCrateLogger {
    pub fn new() -> Self {
        Self
    }
}

impl Logger for LogCrateLogger {
    fn trace(&self, target: &str, message: &str) {
        log::trace!(target: target, "{}", message);
    }
    fn debug(&self, target: &str, message: &str) {
        log::debug!(target: target, "{}", message);
    }

    fn info(&self, target: &str, message: &str) {
        log::info!(target: target, "{}", message);
    }

    fn warn(&self, target: &str, message: &str) {
        log::warn!(target: target, "{}", message);
    }

    fn error(&self, target: &str, message: &str) {
        log::error!(target: target, "{}", message);
    }
}
