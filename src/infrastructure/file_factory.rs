use crate::domain::entities::file::File;
use crate::domain::entities::file::FileKind;
use crate::domain::services::file_factory::FileFactory;
use crate::domain::value_objects::Compression;
use std::ffi::OsStr;
use std::hash::Hasher;
use std::io::Read;
use std::path::Path;
use std::path::PathBuf;
use twox_hash::XxHash64;

#[derive(Debug, Clone)]
pub struct FileFactoryImpl {
    settings: FileFactorySettings,
}

#[derive(Debug, Clone)]
pub struct FileFactorySettings {
    relative_path_to_content_root: PathBuf,
}

impl FileFactorySettings {
    pub fn new(relative_path_to_content_root: PathBuf) -> Self {
        Self {
            relative_path_to_content_root,
        }
    }
}

impl FileFactoryImpl {
    pub fn new(settings: FileFactorySettings) -> Self {
        Self { settings }
    }
}

impl FileFactory for FileFactoryImpl {
    type Error = FileFactoryError;

    fn create_file_from_path<P: AsRef<Path>>(
        &self,
        path: P,
        kind: FileKind,
    ) -> Result<File, Self::Error> {
        let path: PathBuf = path.as_ref().to_path_buf();

        let size: u64 = path
            .metadata()
            .map_err(|error| {
                FileFactoryError::FileMetadataError(format!(
                    "Error getting file metadata: {}",
                    error
                ))
            })?
            .len();

        let hash = hash_file(&path)?;

        let content_root_path = Path::new(&self.settings.relative_path_to_content_root);
        let content_root_path_components: Vec<&OsStr> = content_root_path.iter().collect();
        let file_path_components: Vec<&OsStr> = path.iter().collect();
        let mut preserved_path: Vec<String> = Vec::with_capacity(file_path_components.len());

        match kind {
            FileKind::ContentFile => {
                for (index, file_path_component) in file_path_components.iter().enumerate() {
                    if let Some(content_root_component) = content_root_path_components.get(index) {
                        if content_root_component != file_path_component {
                            return Err(FileFactoryError::FilePathError(format!(
                                "File path {} is not a child of content root: {}",
                                path.display(),
                                content_root_path.display()
                            )));
                        }
                    } else {
                        preserved_path.push(
                            file_path_component
                                .to_str()
                                .ok_or_else(|| {
                                    FileFactoryError::FilePathError(
                                        "File path component is not valid UTF-8".to_string(),
                                    )
                                })?
                                .to_string(),
                        );
                    }
                }
            }
            FileKind::DatabaseContentDump | FileKind::DatabaseStructureDump => {
                preserved_path.push(
                    file_path_components
                        .last()
                        .ok_or_else(|| {
                            FileFactoryError::FilePathError(
                                "File path does not have a file name".to_string(),
                            )
                        })?
                        .to_str()
                        .ok_or_else(|| {
                            FileFactoryError::FilePathError(
                                "File path component is not valid UTF-8".to_string(),
                            )
                        })?
                        .to_string(),
                );
            }
        }

        let first_bytes = std::fs::read(&path)?
            .iter()
            .take(2)
            .cloned()
            .collect::<Vec<u8>>();
        let compression: Compression = Compression::from(first_bytes.as_slice());

        Ok(File::new(
            path,
            size,
            hash,
            kind,
            compression,
            preserved_path,
        ))
    }
}

fn hash_file<P: AsRef<Path>>(file_path: P) -> Result<String, std::io::Error> {
    let mut file = std::fs::File::open(file_path)?;
    let mut content = Vec::new();
    file.read_to_end(&mut content)?;

    let mut hasher = XxHash64::with_seed(0);
    hasher.write(&content);
    let hash = hasher.finish().to_string();

    Ok(hash)
}

#[derive(Debug)]
pub enum FileFactoryError {
    FileNameError(String),
    FilePathError(String),
    FileMetadataError(String),
    IoError(std::io::Error),
}

impl std::fmt::Display for FileFactoryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            FileFactoryError::FileNameError(message) => {
                write!(f, "File name error: {}", message)
            }
            FileFactoryError::FilePathError(message) => {
                write!(f, "File path error: {}", message)
            }
            FileFactoryError::FileMetadataError(message) => {
                write!(f, "File metadata error: {}", message)
            }
            FileFactoryError::IoError(error) => write!(f, "IO error: {}", error),
        }
    }
}

impl std::error::Error for FileFactoryError {}

impl From<std::io::Error> for FileFactoryError {
    fn from(e: std::io::Error) -> Self {
        FileFactoryError::IoError(e)
    }
}
