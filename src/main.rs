use std::path::PathBuf;

use clap::Parser;
use dev_storage_client::{
    adapters::{
        controllers::cli_controller::{CliArgs, CliController},
        gateways::git_repository::GitRepositoryGatewayImpl,
        gateways::holochain_storage::HolochainGateway,
    },
    application::use_cases::{
        create_snapshot::CreateSnapshotUseCase,
        create_snapshot_package::CreateSnapshotPackageUseCase,
        push_snapshot_to_storage::PushSnapshotToStorageUseCase,
    },
    domain::services::logger::Logger,
    infrastructure::{
        configuration::{AppConfig, ConfigLoader},
        content_dir_scanner::{ContentDirScannerImpl, ContentDirScannerImplConfig},
        database_dumper::MySqlDumper,
        file_compressor::GzipCompressor,
        file_factory::{FileFactoryImpl, FileFactorySettings},
        logger::LogCrateLogger,
        tmp_dir_creator::TmpDirCreatorImpl,
    },
};
use tracing::Level;
use tracing_subscriber::fmt::format::Format;

// TODO: pull from storage use case

fn main() {
    let cli_args: CliArgs = CliArgs::parse();

    // Logger
    let log_level = match cli_args.verbose {
        0 => Level::INFO,
        1 => Level::DEBUG,
        _ => Level::TRACE,
    };
    let log_format = Format::default()
        .without_time()
        .with_target(log_level >= Level::DEBUG);
    let tracing_subscriber = tracing_subscriber::fmt()
        .event_format(log_format)
        .with_max_level(log_level)
        .finish();
    tracing_log::LogTracer::init().expect("failed to set global log compatibility layer");
    tracing::subscriber::set_global_default(tracing_subscriber)
        .expect("setting default subscriber failed");

    // Configuration
    let working_directory = PathBuf::from(".");
    let tmp_base_directory = working_directory.join(".dev-storage");
    if tmp_base_directory.exists() {
        std::fs::remove_dir_all(&tmp_base_directory).expect("failed to remove tmp directory");
        std::fs::create_dir(&tmp_base_directory).expect("failed to create tmp directory");
    } else {
        std::fs::create_dir(&tmp_base_directory).expect("failed to create tmp directory");
    }
    let config_file_path = working_directory.join(".dev-storage-client.json");
    let config_loader = ConfigLoader::new(config_file_path);
    let app_config: AppConfig = match config_loader.load() {
        Ok(config) => config,
        Err(error) => {
            log::error!("failed to load local config: {:?}", error);
            return;
        }
    };

    let file_factory_settings = FileFactorySettings::new(PathBuf::from(
        app_config.relative_path_to_content_root.clone(),
    ));
    let content_directory_path = PathBuf::from(app_config.relative_path_to_content_root.clone());
    let cms_content_file_repository_config =
        ContentDirScannerImplConfig::new(content_directory_path);

    // Services
    let logger = LogCrateLogger::new();
    let tmp_dir_creator = TmpDirCreatorImpl::new(tmp_base_directory.clone());
    let file_compressor = GzipCompressor::new();
    let file_factory = FileFactoryImpl::new(file_factory_settings);
    let database_dumper = MySqlDumper::new(app_config.database.into());

    // Repositories
    let git_repository_gateway = GitRepositoryGatewayImpl::new();
    let content_file_repository = ContentDirScannerImpl::new(cms_content_file_repository_config);

    // Gateways
    let storage_gateway = HolochainGateway::new(app_config.holochain.into());

    // Use cases
    let create_snapshot_use_case = CreateSnapshotUseCase::new(
        git_repository_gateway,
        file_compressor.clone(),
        file_factory,
        content_file_repository.clone(),
        database_dumper.clone(),
        logger.clone(),
        tmp_dir_creator,
    );
    let create_snapshot_package_use_case = CreateSnapshotPackageUseCase::new(
        logger.clone(),
        create_snapshot_use_case.clone(),
        file_compressor.clone(),
    );
    let push_snapshot_to_storage_use_case = PushSnapshotToStorageUseCase::new(
        storage_gateway,
        logger.clone(),
        create_snapshot_use_case,
    );

    // Controller
    let mut cli_controller = CliController::new(
        create_snapshot_package_use_case,
        push_snapshot_to_storage_use_case,
        cli_args,
    );

    if let Err(error) = cli_controller.execute() {
        logger.error(module_path!(), &format!("execution failure: {:?}", error))
    }

    // Cleanup
    if let Err(error) = std::fs::remove_dir_all(tmp_base_directory) {
        logger.error(
            module_path!(),
            &format!("failed to remove tmp directory: {:?}", error),
        )
    }
}
