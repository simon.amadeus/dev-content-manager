use crate::application::use_cases::create_snapshot::CreateSnapshot;
use crate::domain::aggregates::snapshot::Snapshot;
use crate::domain::gateways::remote_storage::RemoteStorage;
use crate::domain::services::logger::Logger;
use std::fmt::Debug;
use std::fmt::Display;

pub trait PushSnapshotToStorage {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn execute(&mut self) -> Result<(), Self::Error>;
}

pub struct PushSnapshotToStorageUseCase<S, G, U>
where
    S: RemoteStorage,
    G: Logger,
    U: CreateSnapshot,
{
    storage_gateway: S,
    logger: G,
    create_snapshot_use_case: U,
}

impl<S, G, U> PushSnapshotToStorageUseCase<S, G, U>
where
    S: RemoteStorage,
    G: Logger,
    U: CreateSnapshot,
{
    pub fn new(storage_gateway: S, logger: G, create_snapshot_use_case: U) -> Self {
        Self {
            storage_gateway,
            logger,
            create_snapshot_use_case,
        }
    }
}

#[derive(Debug)]
pub enum PushSnapshotToStorageError<S: RemoteStorage, U: CreateSnapshot> {
    StorageGatewayError(<S as RemoteStorage>::Error),
    CreateSnapshotError(<U as CreateSnapshot>::Error),
}

impl<S, U> Display for PushSnapshotToStorageError<S, U>
where
    S: RemoteStorage,
    U: CreateSnapshot,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PushSnapshotToStorageError::StorageGatewayError(error) => {
                write!(f, "storage service error: {:#?}", error)
            }
            PushSnapshotToStorageError::CreateSnapshotError(error) => {
                write!(f, "create snapshot use case error: {:#?}", error)
            }
        }
    }
}

impl<S, U> std::error::Error for PushSnapshotToStorageError<S, U>
where
    S: RemoteStorage + Debug,
    U: CreateSnapshot + Debug,
{
}

impl<S, G, U> PushSnapshotToStorage for PushSnapshotToStorageUseCase<S, G, U>
where
    S: RemoteStorage + Debug,
    G: Logger + Debug,
    U: CreateSnapshot + Debug,
{
    type Error = PushSnapshotToStorageError<S, U>;

    fn execute(&mut self) -> Result<(), Self::Error> {
        self.logger.debug(
            module_path!(),
            "started: push commit-related files to storage use case",
        );

        self.logger.info(module_path!(), "creating list of files");
        let snapshot: Snapshot = match self.create_snapshot_use_case.execute() {
            Ok(files) if files.is_empty() => {
                self.logger.info(module_path!(), "no files found");
                return Ok(());
            }
            Ok(files) => {
                self.logger
                    .debug(module_path!(), "successfully created list of files");
                self.logger
                    .trace(module_path!(), &format!("list of files: {:#?}", files));
                files
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("error while creating list of files: {:#?}", error),
                );
                return Err(PushSnapshotToStorageError::CreateSnapshotError(error));
            }
        };

        self.logger.debug(
            module_path!(),
            &format!("list contains {} files", snapshot.len()),
        );

        self.logger
            .info(module_path!(), "push files to storage service");

        match self.storage_gateway.push_snapshot(snapshot) {
            Ok(_) => {}
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("error while pushing file to storage: {:#?}", &error),
                );
                return Err(PushSnapshotToStorageError::StorageGatewayError(error));
            }
        };

        self.logger.debug(
            module_path!(),
            "successfully pushed files to storage service",
        );

        self.logger.debug(
            module_path!(),
            "finished: push commit-related files to storage",
        );
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::application::use_cases::create_snapshot::tests::MockCreateSnapshotUseCase;
    use crate::domain::aggregates::snapshot::Snapshot;
    use crate::domain::entities::commit_hash::CommitHash;
    use crate::domain::entities::file::{File, FileKind};
    use crate::domain::gateways::remote_storage::tests::MockStorageGatewayBuilder;
    use crate::domain::services::logger::tests::MockLogger;
    use crate::domain::value_objects::Compression;
    use std::path::PathBuf;
    use uuid::Uuid;

    fn get_tmp_dir() -> PathBuf {
        let random_dir_name = Uuid::new_v4().to_string();
        let tmp_dir_path = std::env::temp_dir().join(random_dir_name);
        std::fs::create_dir_all(&tmp_dir_path).expect("failed to create tmp dir");

        tmp_dir_path
    }

    fn get_content_file_list() -> Vec<File> {
        let mut file_list = vec![];
        file_list.push(get_mocked_content_file());
        file_list.push(get_mocked_content_file());

        file_list
    }
    fn get_snapshot() -> Snapshot {
        let content_files = get_content_file_list();
        let tmp_dir = get_tmp_dir();
        let commit_hash = CommitHash::new("test_hash".to_string());
        let mut snapshot = Snapshot::new(commit_hash, tmp_dir);
        for file in content_files {
            snapshot.add_file(file).expect("failed to add file to list");
        }

        snapshot
            .add_file(get_mocked_database_content_dump_file())
            .expect("failed to add content dump file to list");
        snapshot
            .add_file(get_mocked_database_structure_dump_file())
            .expect("failed to add structure dump file to list");

        snapshot
    }
    fn get_mocked_content_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::ContentFile,
            Compression::Gzip,
            vec![],
        );
        file
    }
    fn get_mocked_database_content_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseContentDump,
            Compression::None,
            vec![file_name],
        );

        file
    }
    fn get_mocked_database_structure_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseStructureDump,
            Compression::None,
            vec![file_name],
        );

        file
    }

    #[test]
    fn returns_storage_service_error_when_storage_service_returns_error() {
        let storage_service = MockStorageGatewayBuilder::new().with_error().build();
        let snapshot = get_snapshot();
        let create_snapshot_use_case = MockCreateSnapshotUseCase::new(true, snapshot);

        let mut use_case = PushSnapshotToStorageUseCase::new(
            storage_service,
            MockLogger::new(),
            create_snapshot_use_case,
        );

        match use_case.execute() {
            Err(PushSnapshotToStorageError::StorageGatewayError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn does_nothing_if_snapshot_is_empty() {
        let commit_hash_string = "test_hash".to_string();
        let storage_service = MockStorageGatewayBuilder::new().build();
        let snapshot = Snapshot::new(CommitHash::new(commit_hash_string), get_tmp_dir());
        let create_snapshot_use_case = MockCreateSnapshotUseCase::new(true, snapshot);

        let mut use_case = PushSnapshotToStorageUseCase::new(
            storage_service,
            MockLogger::new(),
            create_snapshot_use_case,
        );

        let result = use_case.execute();

        assert!(result.is_ok());
        assert!(!use_case.storage_gateway.push_snapshot_was_called());
    }

    #[test]
    fn happy_path() {
        let storage_service = MockStorageGatewayBuilder::new().build();
        let snapshot = get_snapshot();
        let create_snapshot_use_case = MockCreateSnapshotUseCase::new(true, snapshot);

        let mut use_case = PushSnapshotToStorageUseCase::new(
            storage_service,
            MockLogger::new(),
            create_snapshot_use_case,
        );

        let result = use_case.execute();

        assert!(result.is_ok());
        assert!(use_case.storage_gateway.push_snapshot_was_called());
    }
}
