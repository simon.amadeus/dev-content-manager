use crate::domain::aggregates::snapshot::Snapshot;
use crate::domain::gateways::git_repository::GitRepository;
use crate::domain::gateways::remote_storage::RemoteStorage;
use crate::domain::services::logger::Logger;
use crate::domain::services::tmp_dir_creator::TmpDirCreator;
use std::fmt::Debug;
use std::fmt::Display;

use super::create_snapshot_package::CreateSnapshotPackage;

pub trait PullCurrentSnapshotFromStorage {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn execute(&mut self) -> Result<(), Self::Error>;
}

pub struct PullCurrentSnapshotFromStorageUseCase<S, G, K, T, U>
where
    S: RemoteStorage,
    K: GitRepository,
    G: Logger,
    T: TmpDirCreator,
    U: CreateSnapshotPackage,
{
    storage_gateway: S,
    git_repository_gateway: K,
    tmp_dir_creator: T,
    logger: G,
    create_snapshot_package_use_case: U,
    // TODO: add restore snapshot use case
}

impl<S, G, K, T, U> PullCurrentSnapshotFromStorageUseCase<S, G, K, T, U>
where
    S: RemoteStorage,
    K: GitRepository,
    G: Logger,
    T: TmpDirCreator,
    U: CreateSnapshotPackage,
{
    pub fn new(
        storage_gateway: S,
        git_repository_gateway: K,
        tmp_dir_creator: T,
        logger: G,
        create_snapshot_package_use_case: U,
    ) -> Self {
        Self {
            storage_gateway,
            git_repository_gateway,
            tmp_dir_creator,
            logger,
            create_snapshot_package_use_case,
        }
    }
}

#[derive(Debug)]
pub enum PullCurrentSnapshotFromStorageError<
    S: RemoteStorage,
    K: GitRepository,
    T: TmpDirCreator,
    U: CreateSnapshotPackage,
> {
    StorageGatewayError(<S as RemoteStorage>::Error),
    GitRepositoryGatewayError(<K as GitRepository>::Error),
    TmpDirCreatorError(<T as TmpDirCreator>::Error),
    CreateSnapshotPackageUseCaseError(<U as CreateSnapshotPackage>::Error),
}

impl<S, K, T, U> Display for PullCurrentSnapshotFromStorageError<S, K, T, U>
where
    S: RemoteStorage,
    K: GitRepository,
    T: TmpDirCreator,
    U: CreateSnapshotPackage,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PullCurrentSnapshotFromStorageError::StorageGatewayError(error) => {
                write!(f, "storage service error: {:#?}", error)
            }
            PullCurrentSnapshotFromStorageError::GitRepositoryGatewayError(error) => {
                write!(f, "git repository error: {:#?}", error)
            }
            PullCurrentSnapshotFromStorageError::TmpDirCreatorError(error) => {
                write!(f, "tmp dir creator error: {:#?}", error)
            }
            PullCurrentSnapshotFromStorageError::CreateSnapshotPackageUseCaseError(error) => {
                write!(f, "create snapshot package use case error: {:#?}", error)
            }
        }
    }
}

impl<S, K, T, U> std::error::Error for PullCurrentSnapshotFromStorageError<S, K, T, U>
where
    S: RemoteStorage + Debug,
    K: GitRepository + Debug,
    T: TmpDirCreator + Debug,
    U: CreateSnapshotPackage + Debug,
{
}

impl<S, G, K, T, U> PullCurrentSnapshotFromStorage
    for PullCurrentSnapshotFromStorageUseCase<S, G, K, T, U>
where
    S: RemoteStorage + Debug,
    G: Logger + Debug,
    K: GitRepository + Debug,
    T: TmpDirCreator + Debug,
    U: CreateSnapshotPackage + Debug,
{
    type Error = PullCurrentSnapshotFromStorageError<S, K, T, U>;

    fn execute(&mut self) -> Result<(), Self::Error> {
        self.logger.debug(
            module_path!(),
            "started: pull current snapshot from storage",
        );

        self.logger.debug(
            module_path!(),
            "finished: pull current snapshot from storage",
        );
        unimplemented!("Pull current snapshot from storage use case not implemented")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::application::use_cases::create_snapshot_package::tests::MockCreateSnapshotPackageUseCaseBuilder;
    use crate::domain::aggregates::snapshot::Snapshot;
    use crate::domain::entities::commit_hash::CommitHash;
    use crate::domain::entities::file::{File, FileKind};
    use crate::domain::gateways::git_repository::tests::MockGitRepositoryGatewayBuilder;
    use crate::domain::gateways::remote_storage::tests::MockStorageGatewayBuilder;
    use crate::domain::services::logger::tests::MockLogger;
    use crate::domain::services::tmp_dir_creator::tests::MockTmpDirCreatorBuilder;
    use crate::domain::value_objects::Compression;
    use std::path::PathBuf;
    use uuid::Uuid;

    fn get_tmp_dir() -> PathBuf {
        let random_dir_name = Uuid::new_v4().to_string();
        let tmp_dir_path = std::env::temp_dir().join(random_dir_name);
        std::fs::create_dir_all(&tmp_dir_path).expect("failed to create tmp dir");

        tmp_dir_path
    }

    fn get_content_file_list() -> Vec<File> {
        let mut file_list = vec![];
        file_list.push(get_mocked_content_file());
        file_list.push(get_mocked_content_file());

        file_list
    }
    fn get_snapshot() -> Snapshot {
        let content_files = get_content_file_list();
        let tmp_dir = get_tmp_dir();
        let commit_hash = CommitHash::new("test_hash".to_string());
        let mut snapshot = Snapshot::new(commit_hash, tmp_dir);
        for file in content_files {
            snapshot.add_file(file).expect("failed to add file to list");
        }

        snapshot
            .add_file(get_mocked_database_content_dump_file())
            .expect("failed to add content dump file to list");
        snapshot
            .add_file(get_mocked_database_structure_dump_file())
            .expect("failed to add structure dump file to list");

        snapshot
    }
    fn get_mocked_content_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::ContentFile,
            Compression::Gzip,
            vec![],
        );
        file
    }
    fn get_mocked_database_content_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseContentDump,
            Compression::None,
            vec![file_name],
        );

        file
    }
    fn get_mocked_database_structure_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseStructureDump,
            Compression::None,
            vec![file_name],
        );

        file
    }

    #[test]
    fn returns_storage_service_error_when_storage_service_returns_error() {
        let storage_service = MockStorageGatewayBuilder::new().with_error().build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_hash".to_string())
            .build();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new().build();
        let create_snapshot_package_use_case =
            MockCreateSnapshotPackageUseCaseBuilder::new().build();
        let mut pull_current_snapshot_use_case = PullCurrentSnapshotFromStorageUseCase::new(
            storage_service,
            git_repository_gateway,
            tmp_dir_creator,
            MockLogger::new(),
            create_snapshot_package_use_case,
        );

        match pull_current_snapshot_use_case.execute() {
            Err(PullCurrentSnapshotFromStorageError::StorageGatewayError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn returns_tmp_dir_creator_error_when_tmp_dir_creator_returns_error() {
        let storage_service = MockStorageGatewayBuilder::new().build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_hash".to_string())
            .build();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new().with_error().build();
        let create_snapshot_package_use_case =
            MockCreateSnapshotPackageUseCaseBuilder::new().build();
        let mut pull_current_snapshot_use_case = PullCurrentSnapshotFromStorageUseCase::new(
            storage_service,
            git_repository_gateway,
            tmp_dir_creator,
            MockLogger::new(),
            create_snapshot_package_use_case,
        );

        match pull_current_snapshot_use_case.execute() {
            Err(PullCurrentSnapshotFromStorageError::TmpDirCreatorError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn returns_git_repository_error_when_git_repository_returns_error() {
        let storage_service = MockStorageGatewayBuilder::new().build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new().with_error().build();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new().build();
        let create_snapshot_package_use_case =
            MockCreateSnapshotPackageUseCaseBuilder::new().build();
        let mut pull_current_snapshot_use_case = PullCurrentSnapshotFromStorageUseCase::new(
            storage_service,
            git_repository_gateway,
            tmp_dir_creator,
            MockLogger::new(),
            create_snapshot_package_use_case,
        );

        match pull_current_snapshot_use_case.execute() {
            Err(PullCurrentSnapshotFromStorageError::GitRepositoryGatewayError(_)) => {
                assert!(true)
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn returns_create_snapshot_package_use_case_error_when_create_snapshot_package_use_case_returns_error(
    ) {
        let snapshot = get_snapshot();
        let storage_service = MockStorageGatewayBuilder::new()
            .with_snapshot(snapshot)
            .build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_hash".to_string())
            .build();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new().build();
        let create_snapshot_package_use_case = MockCreateSnapshotPackageUseCaseBuilder::new()
            .with_error()
            .build();
        let mut pull_current_snapshot_use_case = PullCurrentSnapshotFromStorageUseCase::new(
            storage_service,
            git_repository_gateway,
            tmp_dir_creator,
            MockLogger::new(),
            create_snapshot_package_use_case,
        );

        match pull_current_snapshot_use_case.execute() {
            Err(PullCurrentSnapshotFromStorageError::StorageGatewayError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn happy_path() {
        let snapshot = get_snapshot();
        let storage_service = MockStorageGatewayBuilder::new()
            .with_snapshot(snapshot)
            .build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_hash".to_string())
            .build();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new().build();
        let create_snapshot_package_use_case =
            MockCreateSnapshotPackageUseCaseBuilder::new().build();
        let mut pull_current_snapshot_use_case = PullCurrentSnapshotFromStorageUseCase::new(
            storage_service,
            git_repository_gateway,
            tmp_dir_creator,
            MockLogger::new(),
            create_snapshot_package_use_case.clone(),
        );

        let result = pull_current_snapshot_use_case.execute();

        assert!(pull_current_snapshot_use_case
            .git_repository_gateway
            .get_current_commit_hash_was_called());
        assert!(create_snapshot_package_use_case.was_called());
        assert!(pull_current_snapshot_use_case
            .storage_gateway
            .pull_snapshot_was_called());
        assert!(pull_current_snapshot_use_case
            .tmp_dir_creator
            .create_tmp_dir_was_called());
        assert!(result.is_ok());
    }
}
