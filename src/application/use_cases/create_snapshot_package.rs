use crate::application::use_cases::create_snapshot::CreateSnapshot;
use crate::domain::aggregates::snapshot::Snapshot;
use crate::domain::aggregates::snapshot::Structured;
use crate::domain::services::file_compressor::FileCompressor;
use crate::domain::services::logger::Logger;
use std::fmt::Debug;
use std::fmt::Display;
use std::path::Path;

pub trait CreateSnapshotPackage {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn execute(&self) -> Result<(), Self::Error>;
}

pub struct CreateSnapshotPackageUseCase<G, U, C>
where
    G: Logger,
    U: CreateSnapshot,
    C: FileCompressor,
{
    logger: G,
    create_snapshot_use_case: U,
    file_compressor: C,
}

impl<G, U, C> CreateSnapshotPackageUseCase<G, U, C>
where
    G: Logger,
    U: CreateSnapshot,
    C: FileCompressor,
{
    pub fn new(logger: G, create_snapshot_use_case: U, file_compressor: C) -> Self {
        Self {
            logger,
            create_snapshot_use_case,
            file_compressor,
        }
    }
}

#[derive(Debug)]
pub enum CreateSnapshotPackageError<U: CreateSnapshot, C: FileCompressor> {
    CreatesnapshotError(<U as CreateSnapshot>::Error),
    FileCompressorError(<C as FileCompressor>::Error),
    IoError(std::io::Error),
}

impl<U, C> Display for CreateSnapshotPackageError<U, C>
where
    U: CreateSnapshot,
    C: FileCompressor,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CreateSnapshotPackageError::CreatesnapshotError(error) => {
                write!(f, "create snapshot use case error: {:#?}", error)
            }
            CreateSnapshotPackageError::FileCompressorError(error) => {
                write!(f, "file compressor error: {:#?}", error)
            }
            CreateSnapshotPackageError::IoError(error) => write!(f, "io error: {:#?}", error),
        }
    }
}

impl<U, C> std::error::Error for CreateSnapshotPackageError<U, C>
where
    U: CreateSnapshot + Debug,
    C: FileCompressor + Debug,
{
}

impl<G, U, C> CreateSnapshotPackage for CreateSnapshotPackageUseCase<G, U, C>
where
    G: Logger + Debug,
    U: CreateSnapshot + Debug,
    C: FileCompressor + Debug,
{
    type Error = CreateSnapshotPackageError<U, C>;

    fn execute(&self) -> Result<(), Self::Error> {
        self.logger
            .debug(module_path!(), "started: create content package use case");

        self.logger.info(module_path!(), "creating full snapshot");
        let snapshot: Snapshot = match self.create_snapshot_use_case.execute() {
            Ok(snapshot) if snapshot.is_empty() => {
                self.logger.info(module_path!(), "snapshot is empty");
                return Ok(());
            }
            Ok(snapshot) => {
                self.logger
                    .debug(module_path!(), "successfully created snapshot");
                self.logger
                    .trace(module_path!(), &format!("snapshot: {:#?}", snapshot));
                snapshot
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("error while creating snapshot: {:#?}", error),
                );
                return Err(CreateSnapshotPackageError::CreatesnapshotError(error));
            }
        };

        self.logger.info(
            module_path!(),
            &format!("snapshot contains {} files", snapshot.len()),
        );

        self.logger
            .debug(module_path!(), "determine current directory");
        let current_dir = match std::env::current_dir() {
            // TODO: inject current dir
            Ok(dir) => {
                self.logger
                    .debug(module_path!(), "successfully determined current directory");
                self.logger
                    .debug(module_path!(), &format!("current directory: {:#?}", dir));
                dir
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("error getting current dir: {:#?}", error),
                );
                return Err(CreateSnapshotPackageError::IoError(error));
            }
        };

        self.logger
            .debug(module_path!(), "restoring directory structure");
        let snapshot: Snapshot<Structured> = match snapshot.restore_directory_structure() {
            Ok(snapshot) => {
                self.logger
                    .debug(module_path!(), "successfully restored directory structure");
                self.logger
                    .trace(module_path!(), &format!("snapshot: {:#?}", snapshot));
                snapshot
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("error while restoring directory structure: {:#?}", error),
                );
                return Err(CreateSnapshotPackageError::IoError(error));
            }
        };

        self.logger.info(module_path!(), "creating file bundle");
        let files_to_pack: Vec<&Path> = snapshot.all_file_paths();
        match self
            .file_compressor
            .pack_files(&files_to_pack, &current_dir)
        {
            Ok(file) => {
                self.logger.info(
                    module_path!(),
                    &format!("successfully created file bundle: {:#?}", file),
                );
                self.logger
                    .debug(module_path!(), &format!("content package: {:#?}", file));
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!(
                        "error while bundling files into content package: {:#?}",
                        error
                    ),
                );
                return Err(CreateSnapshotPackageError::FileCompressorError(error));
            }
        }

        self.logger
            .debug(module_path!(), "finished: create content package use case");
        Ok(())
    }
}

#[cfg(test)]
pub mod tests {
    use uuid::Uuid;

    use super::*;
    use crate::application::use_cases::create_snapshot::tests::MockCreateSnapshotUseCase;
    use crate::domain::entities::commit_hash::CommitHash;
    use crate::domain::entities::file::{File, FileKind};
    use crate::domain::services::file_compressor::tests::MockFileCompressorBuilder;
    use crate::domain::services::logger::tests::MockLogger;
    use crate::domain::value_objects::Compression;
    use std::cell::RefCell;
    use std::path::PathBuf;

    #[derive(Debug, PartialEq, Eq)]
    pub struct MockCreateSnapshotPackageError;

    impl std::fmt::Display for MockCreateSnapshotPackageError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "mock error")
        }
    }

    impl std::error::Error for MockCreateSnapshotPackageError {}

    #[derive(Debug, Clone)]
    pub struct MockCreateSnapshotPackageUseCase {
        success: bool,
        was_called: RefCell<bool>,
    }

    impl MockCreateSnapshotPackageUseCase {
        pub fn new(success: bool) -> Self {
            Self {
                success,
                was_called: RefCell::new(false),
            }
        }

        pub fn was_called(&self) -> bool {
            *self.was_called.borrow()
        }
    }

    impl CreateSnapshotPackage for MockCreateSnapshotPackageUseCase {
        type Error = MockCreateSnapshotPackageError;

        fn execute(&self) -> Result<(), Self::Error> {
            *self.was_called.borrow_mut() = true;
            if self.success {
                Ok(())
            } else {
                Err(MockCreateSnapshotPackageError)
            }
        }
    }

    #[derive(Debug)]
    pub struct MockCreateSnapshotPackageUseCaseBuilder {
        success: bool,
    }

    impl MockCreateSnapshotPackageUseCaseBuilder {
        pub fn new() -> Self {
            Self { success: true }
        }

        pub fn with_error(mut self) -> Self {
            self.success = false;
            self
        }

        pub fn build(self) -> MockCreateSnapshotPackageUseCase {
            MockCreateSnapshotPackageUseCase::new(self.success)
        }
    }

    fn get_compressed_file_path() -> PathBuf {
        let tmp_dir = get_tmp_dir();
        let file_name = Uuid::new_v4().to_string();
        let file_path = tmp_dir.join(format!("{}.{}", file_name, "gz"));
        std::fs::write(&file_path, "test_content").expect("failed to create file");

        file_path
    }
    fn get_tmp_dir() -> PathBuf {
        let random_dir_name = Uuid::new_v4().to_string();
        let tmp_dir_path = std::env::temp_dir().join(random_dir_name);
        std::fs::create_dir_all(&tmp_dir_path).expect("failed to create tmp dir");

        tmp_dir_path
    }

    fn get_content_file_list() -> Vec<File> {
        let mut file_list = vec![];
        file_list.push(get_mocked_content_file());
        file_list.push(get_mocked_content_file());

        file_list
    }
    fn get_snapshot() -> Snapshot {
        let content_files = get_content_file_list();
        let tmp_dir = get_tmp_dir();
        let commit_hash = CommitHash::new("test_hash".to_string());
        let mut snapshot = Snapshot::new(commit_hash, tmp_dir);
        for file in content_files {
            snapshot.add_file(file).expect("failed to add file to list");
        }

        snapshot
            .add_file(get_mocked_database_content_dump_file())
            .expect("failed to add database content dump file to list");
        snapshot
            .add_file(get_mocked_database_structure_dump_file())
            .expect("failed to add database structure dump file to list");

        snapshot
    }
    fn get_mocked_content_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let preserved_path_segments = vec!["test".to_string(), "test2".to_string(), file_name];
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::ContentFile,
            Compression::None,
            preserved_path_segments,
        );

        file
    }
    fn get_mocked_database_content_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseContentDump,
            Compression::Gzip,
            vec![file_name],
        );

        file
    }
    fn get_mocked_database_structure_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseStructureDump,
            Compression::Gzip,
            vec![file_name],
        );

        file
    }

    #[test]
    fn does_nothing_if_snapshot_is_empty() {
        let snapshot = Snapshot::new(CommitHash::new("test_hash".to_string()), get_tmp_dir());
        let create_snapshot_use_case = MockCreateSnapshotUseCase::new(true, snapshot);
        let file_compressor = MockFileCompressorBuilder::new().build();

        let use_case = CreateSnapshotPackageUseCase::new(
            MockLogger::new(),
            create_snapshot_use_case,
            file_compressor,
        );

        let result = use_case.execute();

        assert!(result.is_ok());
        assert!(!use_case.file_compressor.compress_file_was_called());
    }

    #[test]
    fn happy_path() {
        let snapshot = get_snapshot();
        let create_snapshot_use_case = MockCreateSnapshotUseCase::new(true, snapshot);
        let file_compressor = MockFileCompressorBuilder::new()
            .with_output_file(get_compressed_file_path())
            .build();

        let use_case = CreateSnapshotPackageUseCase::new(
            MockLogger::new(),
            create_snapshot_use_case,
            file_compressor,
        );

        let result = use_case.execute();

        assert!(result.is_ok());
        assert!(use_case
            .create_snapshot_use_case
            .was_called
            .borrow()
            .clone());
        assert!(use_case.file_compressor.pack_files_was_called());
    }
}
