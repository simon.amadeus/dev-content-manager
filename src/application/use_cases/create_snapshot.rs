use crate::domain::aggregates::snapshot::Snapshot;
use crate::domain::entities::commit_hash::CommitHash;
use crate::domain::entities::file::File;
use crate::domain::entities::file::FileKind;
use crate::domain::gateways::git_repository::GitRepository;
use crate::domain::services::content_dir_scanner::ContentDirScanner;
use crate::domain::services::database_dumper::DatabaseDumper;
use crate::domain::services::file_compressor::FileCompressor;
use crate::domain::services::file_factory::FileFactory;
use crate::domain::services::logger::Logger;
use crate::domain::services::tmp_dir_creator::TmpDirCreator;
use std::fmt::Debug;
use std::fmt::Display;
use std::path::PathBuf;

pub trait CreateSnapshot {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn execute(&self) -> Result<Snapshot, Self::Error>;
}

#[derive(Debug, Clone)]
pub struct CreateSnapshotUseCase<G, C, F, P, D, L, T>
where
    G: GitRepository,
    C: FileCompressor,
    F: FileFactory,
    P: ContentDirScanner,
    D: DatabaseDumper,
    L: Logger,
    T: TmpDirCreator,
{
    git_repository_gateway: G,
    file_compressor: C,
    file_factory: F,
    content_dir_scanner: P,
    database_dump_service: D,
    logger: L,
    tmp_dir_creator: T,
}

impl<G, C, F, P, D, L, T> CreateSnapshotUseCase<G, C, F, P, D, L, T>
where
    G: GitRepository,
    C: FileCompressor,
    F: FileFactory,
    P: ContentDirScanner,
    D: DatabaseDumper,
    L: Logger,
    T: TmpDirCreator,
{
    pub fn new(
        git_repository_gateway: G,
        file_compressor: C,
        file_factory: F,
        content_dir_scanner: P,
        database_dump_service: D,
        logger: L,
        tmp_dir_creator: T,
    ) -> Self {
        Self {
            git_repository_gateway,
            file_compressor,
            file_factory,
            content_dir_scanner,
            database_dump_service,
            logger,
            tmp_dir_creator,
        }
    }
}

#[derive(Debug)]
pub enum CreateSnapshotError<
    G: GitRepository,
    C: FileCompressor,
    F: FileFactory,
    P: ContentDirScanner,
    D: DatabaseDumper,
    T: TmpDirCreator,
> {
    GitRepositoryGatewayError(<G as GitRepository>::Error),
    FileCompressorError(<C as FileCompressor>::Error),
    FileFactoryError(<F as FileFactory>::Error),
    FilePathRepositoryError(<P as ContentDirScanner>::Error),
    DatabaseDumperError(<D as DatabaseDumper>::Error),
    TmpDirCreatorError(<T as TmpDirCreator>::Error),
    IoError(std::io::Error),
}

impl<G, C, F, P, D, T> Display for CreateSnapshotError<G, C, F, P, D, T>
where
    G: GitRepository,
    C: FileCompressor,
    F: FileFactory,
    P: ContentDirScanner,
    D: DatabaseDumper,
    T: TmpDirCreator,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CreateSnapshotError::GitRepositoryGatewayError(error) => {
                write!(f, "git repository gateway error: {:#?}", error)
            }
            CreateSnapshotError::FileCompressorError(error) => {
                write!(f, "file compressor error: {:#?}", error)
            }
            CreateSnapshotError::FileFactoryError(error) => {
                write!(f, "file factory error: {:#?}", error)
            }
            CreateSnapshotError::FilePathRepositoryError(error) => {
                write!(f, "file path repository error: {:#?}", error)
            }
            CreateSnapshotError::DatabaseDumperError(error) => {
                write!(f, "database structure dump service error: {:#?}", error)
            }
            CreateSnapshotError::TmpDirCreatorError(error) => {
                write!(f, "tmp dir creator error: {:#?}", error)
            }
            CreateSnapshotError::IoError(error) => write!(f, "io error: {:#?}", error),
        }
    }
}

impl<G, C, F, P, D, T> std::error::Error for CreateSnapshotError<G, C, F, P, D, T>
where
    G: GitRepository + Debug,
    C: FileCompressor + Debug,
    F: FileFactory + Debug,
    P: ContentDirScanner + Debug,
    D: DatabaseDumper + Debug,
    T: TmpDirCreator + Debug,
{
}

impl<G, C, F, P, D, L, T> CreateSnapshot for CreateSnapshotUseCase<G, C, F, P, D, L, T>
where
    G: GitRepository + Debug,
    C: FileCompressor + Debug,
    F: FileFactory + Debug,
    P: ContentDirScanner + Debug,
    D: DatabaseDumper + Debug,
    L: Logger + Debug,
    T: TmpDirCreator + Debug,
{
    type Error = CreateSnapshotError<G, C, F, P, D, T>;

    fn execute(&self) -> Result<Snapshot, Self::Error> {
        self.logger
            .debug(module_path!(), "started: create full snapshot use case");

        self.logger
            .debug(module_path!(), "getting current commit hash");
        let current_commit_hash: String =
            match self.git_repository_gateway.get_current_commit_hash() {
                Ok(commit_hash) => {
                    self.logger.debug(
                        module_path!(),
                        &format!("current commit hash: {:#?}", &commit_hash),
                    );
                    commit_hash
                }
                Err(error) => {
                    self.logger.error(
                        module_path!(),
                        &format!("git repository gateway error: {:#?}", error),
                    );
                    return Err(CreateSnapshotError::GitRepositoryGatewayError(error));
                }
            };
        let commit_hash = CommitHash::new(current_commit_hash);

        self.logger
            .debug(module_path!(), "getting content files from repository");
        let content_file_paths: Vec<PathBuf> = match self.content_dir_scanner.scan_content_dir() {
            Ok(files) => {
                self.logger
                    .debug(module_path!(), &format!("received {} files", files.len()));
                self.logger
                    .trace(module_path!(), &format!("files: {:#?}", files));
                files
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("content file repository error: {:#?}", error),
                );
                return Err(CreateSnapshotError::FilePathRepositoryError(error));
            }
        };

        let mut content_files: Vec<File> = Vec::with_capacity(content_file_paths.len());

        for path in content_file_paths {
            let content_file = match self
                .file_factory
                .create_file_from_path(&path, FileKind::ContentFile)
            {
                Ok(file) => {
                    self.logger.debug(
                        module_path!(),
                        &format!("successfully created content file: {:#?}", &file),
                    );
                    file
                }
                Err(error) => {
                    self.logger.error(
                        module_path!(),
                        &format!("file factory error: {:#?}", &error),
                    );
                    return Err(CreateSnapshotError::FileFactoryError(error));
                }
            };

            content_files.push(content_file);
        }

        let tmp_dir = match self
            .tmp_dir_creator
            .create_tmp_dir(Some(commit_hash.hash().to_string()))
        {
            Ok(tmp_dir) => {
                self.logger
                    .debug(module_path!(), &format!("tmp dir: {:#?}", &tmp_dir));
                tmp_dir
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("failed to get tmp dir: {:#?}", error),
                );
                return Err(CreateSnapshotError::TmpDirCreatorError(error));
            }
        };

        let mut snapshot = Snapshot::with_capacity(commit_hash, tmp_dir, content_files.len() + 1);

        for content_file in content_files {
            snapshot.add_file(content_file).map_err(|error| {
                self.logger.error(
                    module_path!(),
                    &format!("failed to add content file to snapshot: {:#?}", error),
                );
                CreateSnapshotError::IoError(error)
            })?;
        }

        self.logger
            .info(module_path!(), "exporting database structure dump");
        let uncompressed_database_structure_dump: PathBuf = match self
            .database_dump_service
            .export_structure_dump(&snapshot.tmp_dir())
        {
            Ok(database_dump) => {
                self.logger.debug(
                    module_path!(),
                    "successfully exported database structure dump",
                );
                self.logger.debug(
                    module_path!(),
                    &format!("database structure dump: {:#?}", &database_dump),
                );
                database_dump
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("database dumper error: {:#?}", error),
                );
                return Err(CreateSnapshotError::DatabaseDumperError(error));
            }
        };

        self.logger
            .info(module_path!(), "compressing database structure dump");
        let compressed_database_structure_dump: PathBuf = match self
            .file_compressor
            .compress_file(&uncompressed_database_structure_dump, &snapshot.tmp_dir())
        {
            Ok(compressed_database_dump) => {
                self.logger.debug(
                    module_path!(),
                    "successfully compressed database structure dump",
                );
                self.logger.debug(
                    module_path!(),
                    &format!(
                        "compressed database structure dump: {:#?}",
                        &compressed_database_dump
                    ),
                );
                compressed_database_dump
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("file compressor error: {:#?}", &error),
                );
                return Err(CreateSnapshotError::FileCompressorError(error));
            }
        };

        let compressed_database_structure_dump_file: File =
            match self.file_factory.create_file_from_path(
                &compressed_database_structure_dump,
                FileKind::DatabaseStructureDump,
            ) {
                Ok(file) => {
                    self.logger.debug(
                        module_path!(),
                        "successfully created compressed database structure dump file",
                    );
                    self.logger.debug(
                        module_path!(),
                        &format!("compressed database structure dump file: {:#?}", &file),
                    );
                    file
                }
                Err(error) => {
                    self.logger.error(
                        module_path!(),
                        &format!("file factory error: {:#?}", &error),
                    );
                    return Err(CreateSnapshotError::FileFactoryError(error));
                }
            };

        self.logger.info(
            module_path!(),
            "add compressed database structure dump to snapshot",
        );
        snapshot
            .add_file(compressed_database_structure_dump_file)
            .map_err(|error| {
                self.logger.error(
                    module_path!(),
                    &format!(
                        "failed to add compressed database structure dump to snapshot: {:#?}",
                        error
                    ),
                );
                CreateSnapshotError::IoError(error)
            })?;
        self.logger.debug(
            module_path!(),
            "successfully added compressed database structure dump to snapshot",
        );

        self.logger
            .info(module_path!(), "exporting database content dump");
        let uncompressed_database_content_dump: PathBuf = match self
            .database_dump_service
            .export_content_dump(&snapshot.tmp_dir())
        {
            Ok(database_dump) => {
                self.logger.debug(
                    module_path!(),
                    "successfully exported database content dump",
                );
                self.logger.debug(
                    module_path!(),
                    &format!("database content dump: {:#?}", &database_dump),
                );
                database_dump
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("database dumper error: {:#?}", error),
                );
                return Err(CreateSnapshotError::DatabaseDumperError(error));
            }
        };

        self.logger
            .info(module_path!(), "compressing database content dump");
        let compressed_database_content_dump: PathBuf = match self
            .file_compressor
            .compress_file(&uncompressed_database_content_dump, &snapshot.tmp_dir())
        {
            Ok(compressed_database_dump) => {
                self.logger.debug(
                    module_path!(),
                    "successfully compressed database content dump",
                );
                self.logger.debug(
                    module_path!(),
                    &format!(
                        "compressed database content dump: {:#?}",
                        &compressed_database_dump
                    ),
                );
                compressed_database_dump
            }
            Err(error) => {
                self.logger.error(
                    module_path!(),
                    &format!("file compressor error: {:#?}", &error),
                );
                return Err(CreateSnapshotError::FileCompressorError(error));
            }
        };

        let compressed_database_content_dump_file: File =
            match self.file_factory.create_file_from_path(
                &compressed_database_content_dump,
                FileKind::DatabaseContentDump,
            ) {
                Ok(file) => {
                    self.logger.debug(
                        module_path!(),
                        "successfully created compressed database content dump file",
                    );
                    self.logger.debug(
                        module_path!(),
                        &format!("compressed database content dump file: {:#?}", &file),
                    );
                    file
                }
                Err(error) => {
                    self.logger.error(
                        module_path!(),
                        &format!("file factory error: {:#?}", &error),
                    );
                    return Err(CreateSnapshotError::FileFactoryError(error));
                }
            };

        self.logger.info(
            module_path!(),
            "add compressed database content dump to snapshot",
        );
        snapshot
            .add_file(compressed_database_content_dump_file)
            .map_err(|error| {
                self.logger.error(
                    module_path!(),
                    &format!("failed to add compressed dump to snapshot: {:#?}", error),
                );
                CreateSnapshotError::IoError(error)
            })?;
        self.logger.debug(
            module_path!(),
            "successfully added compressed database content dump to snapshot",
        );

        self.logger
            .info(module_path!(), "removing uncompressed files");
        // TODO: opt: file could be removed by an injected service
        std::fs::remove_file(&uncompressed_database_structure_dump).map_err(|error| {
            self.logger.error(
                module_path!(),
                &format!("failed to remove uncompressed file: {:#?}", error),
            );
            CreateSnapshotError::IoError(error)
        })?;
        self.logger.debug(
            module_path!(),
            "successfully removed uncompressed database structure dump",
        );

        // TODO: opt: file could be removed by an injected service
        std::fs::remove_file(&uncompressed_database_content_dump).map_err(|error| {
            self.logger.error(
                module_path!(),
                &format!("failed to remove uncompressed file: {:#?}", error),
            );
            CreateSnapshotError::IoError(error)
        })?;
        self.logger.debug(
            module_path!(),
            "successfully removed uncompressed database content dump",
        );

        self.logger
            .debug(module_path!(), "finished: create full snapshot use case");

        Ok(snapshot)
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::domain::entities::file::FileKind;
    use crate::domain::gateways::git_repository::tests::MockGitRepositoryGatewayBuilder;
    use crate::domain::services::content_dir_scanner::tests::MockContentDirScannerBuilder;
    use crate::domain::services::database_dumper::tests::MockDatabaseDumperBuilder;
    use crate::domain::services::file_compressor::tests::MockFileCompressorBuilder;
    use crate::domain::services::file_factory::tests::MockFileFactoryBuilder;
    use crate::domain::services::logger::tests::MockLogger;
    use crate::domain::services::tmp_dir_creator::tests::MockTmpDirCreatorBuilder;
    use crate::domain::value_objects::Compression;
    use std::cell::RefCell;
    use std::path::PathBuf;
    use uuid::Uuid;

    #[derive(Debug, PartialEq, Eq)]
    pub struct MockCreateSnapshotError;

    impl std::fmt::Display for MockCreateSnapshotError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "mock error")
        }
    }

    impl std::error::Error for MockCreateSnapshotError {}

    #[derive(Debug)]
    pub struct MockCreateSnapshotUseCase {
        success: bool,
        pub was_called: RefCell<bool>,
        snapshot: Snapshot,
    }

    impl MockCreateSnapshotUseCase {
        pub fn new(success: bool, snapshot: Snapshot) -> Self {
            Self {
                success,
                was_called: RefCell::new(false),
                snapshot,
            }
        }
    }

    impl CreateSnapshot for MockCreateSnapshotUseCase {
        type Error = MockCreateSnapshotError;

        fn execute(&self) -> Result<Snapshot, Self::Error> {
            *self.was_called.borrow_mut() = true;
            if self.success {
                Ok(self.snapshot.clone())
            } else {
                Err(MockCreateSnapshotError)
            }
        }
    }

    fn get_compressed_file_path() -> PathBuf {
        let tmp_dir = get_tmp_dir();
        let file_name = Uuid::new_v4().to_string();
        let file_path = tmp_dir.join(format!("{}.{}", file_name, "sql.gz"));
        std::fs::write(&file_path, "test_content").expect("failed to create file");

        file_path
    }
    fn get_tmp_dir() -> PathBuf {
        let random_dir_name = Uuid::new_v4().to_string();
        let tmp_dir_path = std::env::temp_dir().join(random_dir_name);
        std::fs::create_dir_all(&tmp_dir_path).expect("failed to create tmp dir");

        tmp_dir_path
    }

    fn get_content_file_paths() -> Vec<PathBuf> {
        let mut file_list = vec![];
        file_list.push(get_mocked_content_file().path().clone());
        file_list.push(get_mocked_content_file().path().clone());

        file_list
    }

    fn get_mocked_content_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let preserved_path_segments = vec![
            "test".to_string(),
            "test2".to_string(),
            "test3".to_string(),
            file_name.clone(),
        ];
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::ContentFile,
            Compression::None,
            preserved_path_segments,
        );

        file
    }
    fn get_mocked_database_structure_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseStructureDump,
            Compression::None,
            vec![file_name],
        );

        file
    }
    fn get_mocked_database_content_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseContentDump,
            Compression::None,
            vec![file_name],
        );

        file
    }

    #[test]
    fn returns_content_dir_scanner_error_when_content_dir_scanner_returns_error() {
        let file_compressor = MockFileCompressorBuilder::new().build();
        let content_dir_scanner = MockContentDirScannerBuilder::new().with_error().build();
        let mocked_structure_dump_file = get_mocked_database_structure_dump_file();
        let database_dump_service = MockDatabaseDumperBuilder::new()
            .with_mocked_structure_dump(mocked_structure_dump_file.path().clone())
            .build();
        let file = get_mocked_content_file();
        let file_factory = MockFileFactoryBuilder::new().with_file(file).build();
        let tmp_dir = get_tmp_dir();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new()
            .with_tmp_dir(tmp_dir)
            .build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_commit".to_string())
            .build();
        let use_case = CreateSnapshotUseCase::new(
            git_repository_gateway,
            file_compressor,
            file_factory,
            content_dir_scanner,
            database_dump_service,
            MockLogger::new(),
            tmp_dir_creator,
        );

        match use_case.execute() {
            Err(CreateSnapshotError::FilePathRepositoryError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn returns_file_compressor_error_when_file_compressor_returns_error() {
        let file_compressor = MockFileCompressorBuilder::new().with_error().build();
        let content_dir_scanner = MockContentDirScannerBuilder::new()
            .with_files(get_content_file_paths())
            .build();
        let mocked_structure_dump_file = get_mocked_database_structure_dump_file();
        let database_dump_service = MockDatabaseDumperBuilder::new()
            .with_mocked_structure_dump(mocked_structure_dump_file.path().clone())
            .build();
        let file = get_mocked_content_file();
        let file_factory = MockFileFactoryBuilder::new().with_file(file).build();
        let tmp_dir = get_tmp_dir();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new()
            .with_tmp_dir(tmp_dir)
            .build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_commit".to_string())
            .build();
        let use_case = CreateSnapshotUseCase::new(
            git_repository_gateway,
            file_compressor,
            file_factory,
            content_dir_scanner,
            database_dump_service,
            MockLogger::new(),
            tmp_dir_creator,
        );

        match use_case.execute() {
            Err(CreateSnapshotError::FileCompressorError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn returns_database_dump_service_error_when_database_dump_service_returns_error() {
        let file_compressor = MockFileCompressorBuilder::new().build();
        let content_dir_scanner = MockContentDirScannerBuilder::new()
            .with_files(get_content_file_paths())
            .build();
        let mocked_structure_dump_file = get_mocked_database_structure_dump_file();
        let database_dump_service = MockDatabaseDumperBuilder::new()
            .with_mocked_structure_dump(mocked_structure_dump_file.path().clone())
            .with_error()
            .build();
        let file = get_mocked_content_file();
        let file_factory = MockFileFactoryBuilder::new().with_file(file).build();
        let tmp_dir = get_tmp_dir();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new()
            .with_tmp_dir(tmp_dir)
            .build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_commit".to_string())
            .build();
        let use_case = CreateSnapshotUseCase::new(
            git_repository_gateway,
            file_compressor,
            file_factory,
            content_dir_scanner,
            database_dump_service,
            MockLogger::new(),
            tmp_dir_creator,
        );

        match use_case.execute() {
            Err(CreateSnapshotError::DatabaseDumperError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn returns_file_factory_error_when_file_factory_returns_error() {
        let compressed_file_path = get_compressed_file_path();
        let file_compressor = MockFileCompressorBuilder::new()
            .with_output_file(compressed_file_path)
            .build();
        let content_dir_scanner = MockContentDirScannerBuilder::new()
            .with_files(get_content_file_paths())
            .build();
        let mocked_structure_dump_file = get_mocked_database_structure_dump_file();
        let database_dump_service = MockDatabaseDumperBuilder::new()
            .with_mocked_structure_dump(mocked_structure_dump_file.path().clone())
            .build();
        let file_factory = MockFileFactoryBuilder::new().with_error().build();
        let tmp_dir = get_tmp_dir();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new()
            .with_tmp_dir(tmp_dir)
            .build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_commit".to_string())
            .build();
        let use_case = CreateSnapshotUseCase::new(
            git_repository_gateway,
            file_compressor,
            file_factory,
            content_dir_scanner,
            database_dump_service,
            MockLogger::new(),
            tmp_dir_creator,
        );

        match use_case.execute() {
            Err(CreateSnapshotError::FileFactoryError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn returns_tmp_dir_creator_error_when_tmp_dir_creator_returns_error() {
        let file_compressor = MockFileCompressorBuilder::new().build();
        let content_dir_scanner = MockContentDirScannerBuilder::new()
            .with_files(get_content_file_paths())
            .build();
        let mocked_structure_dump_file = get_mocked_database_structure_dump_file();
        let database_dump_service = MockDatabaseDumperBuilder::new()
            .with_mocked_structure_dump(mocked_structure_dump_file.path().clone())
            .build();
        let file = get_mocked_content_file();
        let file_factory = MockFileFactoryBuilder::new().with_file(file).build();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new().with_error().build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_commit".to_string())
            .build();
        let use_case = CreateSnapshotUseCase::new(
            git_repository_gateway,
            file_compressor,
            file_factory,
            content_dir_scanner,
            database_dump_service,
            MockLogger::new(),
            tmp_dir_creator,
        );

        match use_case.execute() {
            Err(CreateSnapshotError::TmpDirCreatorError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn returns_git_repository_gateway_error_when_git_repository_gateway_returns_error() {
        let file_compressor = MockFileCompressorBuilder::new().build();
        let content_dir_scanner = MockContentDirScannerBuilder::new()
            .with_files(get_content_file_paths())
            .build();
        let mocked_structure_dump_file = get_mocked_database_structure_dump_file();
        let database_dump_service = MockDatabaseDumperBuilder::new()
            .with_mocked_structure_dump(mocked_structure_dump_file.path().clone())
            .build();
        let file = get_mocked_content_file();
        let file_factory = MockFileFactoryBuilder::new().with_file(file).build();
        let tmp_dir = get_tmp_dir();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new()
            .with_tmp_dir(tmp_dir)
            .build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new().with_error().build();
        let use_case = CreateSnapshotUseCase::new(
            git_repository_gateway,
            file_compressor,
            file_factory,
            content_dir_scanner,
            database_dump_service,
            MockLogger::new(),
            tmp_dir_creator,
        );

        match use_case.execute() {
            Err(CreateSnapshotError::GitRepositoryGatewayError(_)) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn happy_path() {
        let compressed_file = get_compressed_file_path();
        let file_compressor = MockFileCompressorBuilder::new()
            .with_output_file(compressed_file)
            .build();
        let content_dir_scanner = MockContentDirScannerBuilder::new()
            .with_files(get_content_file_paths())
            .build();
        let mocked_structure_dump_file = get_mocked_database_structure_dump_file();
        let mocked_content_dump_file = get_mocked_database_content_dump_file();
        let database_dump_service = MockDatabaseDumperBuilder::new()
            .with_mocked_structure_dump(mocked_structure_dump_file.path().clone())
            .with_mocked_content_dump(mocked_content_dump_file.path().clone())
            .build();
        let content_file = get_mocked_content_file();
        let database_content_dump_file = get_mocked_database_content_dump_file();
        let database_structure_dump_file = get_mocked_database_structure_dump_file();
        let file_factory = MockFileFactoryBuilder::new()
            .with_file(content_file)
            .with_file(database_content_dump_file)
            .with_file(database_structure_dump_file)
            .build();
        let tmp_dir = get_tmp_dir();
        let tmp_dir_creator = MockTmpDirCreatorBuilder::new()
            .with_tmp_dir(tmp_dir)
            .build();
        let git_repository_gateway = MockGitRepositoryGatewayBuilder::new()
            .with_current_commit_hash("test_commit".to_string())
            .build();
        let use_case = CreateSnapshotUseCase::new(
            git_repository_gateway,
            file_compressor,
            file_factory,
            content_dir_scanner,
            database_dump_service,
            MockLogger::new(),
            tmp_dir_creator,
        );
        dbg!(&use_case);
        // 2 content files + 1 database structure dump + 1 database content dump
        let expected_amount_of_files = 4;

        let result = use_case.execute();

        assert!(result.is_ok());
        assert!(use_case
            .git_repository_gateway
            .get_current_commit_hash_was_called());
        assert!(use_case.content_dir_scanner.scan_content_dir_was_called());
        assert!(use_case.tmp_dir_creator.create_tmp_dir_was_called());
        assert!(use_case
            .database_dump_service
            .export_structure_dump_was_called());
        assert!(use_case
            .database_dump_service
            .export_content_dump_was_called());
        assert!(use_case.file_compressor.compress_file_was_called());
        assert!(use_case.file_factory.create_file_from_path_was_called());
        assert_eq!(
            use_case.file_factory.called_create_file_from_path_count(),
            expected_amount_of_files
        );
        assert_eq!(
            expected_amount_of_files as usize,
            result
                .expect("failed_to_receive_correct_amount_of_files")
                .len()
        );
    }
}
