use crate::domain::aggregates::snapshot::Snapshot;
use crate::domain::aggregates::snapshot::Structured;
use crate::domain::services::content_dir_scanner::ContentDirScanner;
use crate::domain::services::database_dumper::DatabaseDumper;
use crate::domain::services::logger::Logger;
use std::fmt::Debug;
use std::fmt::Display;

pub trait RestoreSnapshot {
    type Error: std::error::Error + std::fmt::Debug + std::fmt::Display;

    fn execute(&self, snapshot: Snapshot<Structured>) -> Result<(), Self::Error>;
}

#[derive(Debug, Clone)]
pub struct RestoreSnapshotUseCase<P, D, L>
where
    P: ContentDirScanner,
    D: DatabaseDumper,
    L: Logger,
{
    content_dir_scanner: P, // TODO: make content_dir_
    database_dumper: D,
    logger: L,
}

impl<P, D, L> RestoreSnapshotUseCase<P, D, L>
where
    P: ContentDirScanner,
    D: DatabaseDumper,
    L: Logger,
{
    pub fn new(content_dir_scanner: P, database_dump_service: D, logger: L) -> Self {
        Self {
            content_dir_scanner,
            database_dumper: database_dump_service,
            logger,
        }
    }
}

#[derive(Debug)]
pub enum RestoreSnapshotError<P: ContentDirScanner, D: DatabaseDumper> {
    FilePathRepositoryError(<P as ContentDirScanner>::Error),
    DatabaseDumperError(<D as DatabaseDumper>::Error),
    IoError(std::io::Error),
}

impl<P, D> Display for RestoreSnapshotError<P, D>
where
    P: ContentDirScanner,
    D: DatabaseDumper,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            RestoreSnapshotError::FilePathRepositoryError(error) => {
                write!(f, "file path repository error: {:#?}", error)
            }
            RestoreSnapshotError::DatabaseDumperError(error) => {
                write!(f, "database structure dump service error: {:#?}", error)
            }
            RestoreSnapshotError::IoError(error) => write!(f, "io error: {:#?}", error),
        }
    }
}

impl<P, D> std::error::Error for RestoreSnapshotError<P, D>
where
    P: ContentDirScanner + Debug,
    D: DatabaseDumper + Debug,
{
}

impl<P, D, L> RestoreSnapshot for RestoreSnapshotUseCase<P, D, L>
where
    P: ContentDirScanner + Debug,
    D: DatabaseDumper + Debug,
    L: Logger + Debug,
{
    type Error = RestoreSnapshotError<P, D>;

    fn execute(&self, snapshot: Snapshot<Structured>) -> Result<(), Self::Error> {
        self.logger
            .debug(module_path!(), "started: restore snapshot use case");

        self.logger
            .debug(module_path!(), "finished: restore snapshot use case");

        Ok(())
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::domain::entities::file::{File, FileKind};
    use crate::domain::services::content_dir_scanner::tests::MockContentDirScannerBuilder;
    use crate::domain::services::database_dumper::tests::MockDatabaseDumperBuilder;
    use crate::domain::services::logger::tests::MockLogger;
    use crate::domain::value_objects::Compression;
    use std::cell::RefCell;
    use std::path::PathBuf;
    use uuid::Uuid;

    #[derive(Debug, PartialEq, Eq)]
    pub struct MockRestoreSnapshotError;

    impl std::fmt::Display for MockRestoreSnapshotError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "mock error")
        }
    }

    impl std::error::Error for MockRestoreSnapshotError {}

    #[derive(Debug)]
    pub struct MockRestoreSnapshotUseCase {
        success: bool,
        pub was_called: RefCell<bool>,
    }

    impl MockRestoreSnapshotUseCase {
        pub fn new(success: bool) -> Self {
            Self {
                success,
                was_called: RefCell::new(false),
            }
        }
    }

    impl RestoreSnapshot for MockRestoreSnapshotUseCase {
        type Error = MockRestoreSnapshotError;

        fn execute(&self, _snapshot: Snapshot<Structured>) -> Result<(), Self::Error> {
            *self.was_called.borrow_mut() = true;
            if self.success {
                Ok(())
            } else {
                Err(MockRestoreSnapshotError)
            }
        }
    }

    fn get_compressed_file_path() -> PathBuf {
        let tmp_dir = get_tmp_dir();
        let file_name = Uuid::new_v4().to_string();
        let file_path = tmp_dir.join(format!("{}.{}", file_name, "sql.gz"));
        std::fs::write(&file_path, "test_content").expect("failed to create file");

        file_path
    }
    fn get_tmp_dir() -> PathBuf {
        let random_dir_name = Uuid::new_v4().to_string();
        let tmp_dir_path = std::env::temp_dir().join(random_dir_name);
        std::fs::create_dir_all(&tmp_dir_path).expect("failed to create tmp dir");

        tmp_dir_path
    }

    fn get_content_file_paths() -> Vec<PathBuf> {
        let mut file_list = vec![];
        file_list.push(get_mocked_content_file().path().clone());
        file_list.push(get_mocked_content_file().path().clone());

        file_list
    }

    fn get_mocked_content_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let preserved_path_segments = vec![
            "test".to_string(),
            "test2".to_string(),
            "test3".to_string(),
            file_name.clone(),
        ];
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::ContentFile,
            Compression::None,
            preserved_path_segments,
        );

        file
    }
    fn get_mocked_database_structure_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseStructureDump,
            Compression::None,
            vec![file_name],
        );

        file
    }
    fn get_mocked_database_content_dump_file() -> File {
        let file_name = format!("{}.sql", Uuid::new_v4().to_string());
        let file_path = get_tmp_dir().join(&file_name);
        std::fs::write(&file_path, "test_content").expect("failed to create file");
        let file = File::new(
            file_path,
            10,
            "test_hash".to_string(),
            FileKind::DatabaseContentDump,
            Compression::None,
            vec![file_name],
        );

        file
    }

    // #[test]
    // fn happy_path() {
    //     unimplemented!()
    //     // TODO: write tests
    //     // takes snapshot<structured> as input
    //     // do nothing if snapshot is empty

    //     // import database structure dump
    //     // import database content dump

    //     // import content files
    // }
}
